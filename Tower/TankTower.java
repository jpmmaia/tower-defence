package Tower;

import javax.vecmath.Point2f;

/**
 * @author Jo�o
 * @version 1.0
 * @created 21-mai-2014 11:51:32
 */
public class TankTower extends AbstractTower
{
    public static final String TYPE = "Tank Tower";

    public TankTower(ILevel level, Point2f position)
    {
        super(level, position);
        mFactory = TankTowerFactory.getInstance();
        mBaseCost = 5;
    }

    @Override
    public String getType()
    {
        return TYPE;
    }
}
package Tower;

import Target.*;

/**
 * @author Jo�o
 * @version 1.0
 * @created 21-mai-2014 11:51:30
 */
public class IceProjectile extends AbstractProjectile
{
    public static final String TYPE = "Ice Projectile";

    @Override
    public String getType()
    {
        return TYPE;
    }
}
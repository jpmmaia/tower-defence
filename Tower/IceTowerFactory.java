package Tower;

import Target.IDamageable;

import javax.vecmath.Point2f;
import java.util.List;

/**
 * @author Jo�o
 * @version 1.0
 * @created 21-mai-2014 11:51:31
 */
public class IceTowerFactory extends AbstractTowerFactory
{
    static IceTowerFactory mInstance = new IceTowerFactory();

    private IceTowerFactory()
    {
    }

    public static IceTowerFactory getInstance()
    {
        return mInstance;
    }

    @Override
    public AbstractProjectile createProjectile(int damage, Point2f position, IDamageable target, List<IProjectileObserver> observers)
    {
        AbstractProjectile projectile = new IceProjectile();

        projectile.setDamage(damage);
        projectile.setPosition(position);
        projectile.setTarget(target);
        projectile.setObservers(observers);

        return projectile;
    }

    @Override
    public AbstractTower createTower(ILevel level, Point2f position)
    {
        AbstractTower tower = new IceTower(level, position);
        tower.setBaseDamage(7);
        tower.setBaseFireRate(2);

        return tower;
    }
}
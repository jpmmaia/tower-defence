package Tower;

import javax.vecmath.Point2f;

/**
 * Created by João on 21/05/2014.
 */
public class NullTower extends AbstractTower
{
    public final String TYPE = "Null Tower";

    public NullTower()
    {
        super(null, new Point2f(0.0f, 0.0f));
    }

    @Override
    public String getType()
    {
        return TYPE;
    }
}

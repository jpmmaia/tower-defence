package Tower;

/**
 * Observer pattern
 */
public interface IProjectileObserver
{
    void projectileUpdated(AbstractProjectile projectile, String type);
}

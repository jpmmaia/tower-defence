package Tower;

/**
 * @author Jo�o
 * @version 1.0
 * @created 21-mai-2014 11:51:31
 */
public class Level2 implements ILevel
{
    public static final int TYPE = 2;

    @Override
    public int baseCost()
    {
        return 4;
    }

    @Override
    public int damageRate()
    {
        return 2;
    }

    @Override
    public int fireRate()
    {
        return 2;
    }

    @Override
    public void update()
    {
        // TODO
    }

    @Override
    public int getType()
    {
        return TYPE;
    }

}
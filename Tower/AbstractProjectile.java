package Tower;

import Target.*;

import javax.vecmath.Point2f;
import javax.vecmath.Vector2f;
import java.util.ArrayList;
import java.util.List;

/**
 * Abstract Projectile
 * Visitor Pattern -> IDamageable
 * A projectile has a velocity, and can hit targets and damage them
 */
public abstract class AbstractProjectile
{
    protected List<IProjectileObserver> mObservers = new ArrayList<IProjectileObserver>();
    protected IDamageable mTarget;
    protected int mDamage;
    protected Point2f mPosition;
    protected float mVelocity = 0.5f;
    protected boolean mHit = false;

    public AbstractProjectile() {}

    public void update(long deltaMillis)
    {
        if(mTarget == null || mHit)
            return;

        Point2f targetPosition = mTarget.getPosition();
        Vector2f direction = new Vector2f(targetPosition.x - mPosition.x, targetPosition.y - mPosition.y);
        float distance = direction.length();
        direction.normalize();

        float velocity = mVelocity * (float) deltaMillis / 1000.0f;
        if(velocity >= distance)
        {
            mTarget.accept(this);
            mHit = true;
            return;
        }

        direction.scale(velocity);
        mPosition.add(direction);

        notifyProjectileUpdated();
    }
    public void visit(IDamageable damageable)
    {
        damageable.damage(mDamage);
    }

    private void notifyProjectileUpdated()
    {
        for (IProjectileObserver observer : mObservers)
        {
            observer.projectileUpdated(this, getType());
        }
    }

    public IDamageable getTarget()
    {
        return mTarget;
    }
    public void setTarget(IDamageable mTarget)
    {
        this.mTarget = mTarget;
    }
    public int getDamage()
    {
        return mDamage;
    }
    public void setDamage(int damage)
    {
        mDamage = damage;
    }
    public Point2f getPosition()
    {
        return mPosition;
    }
    public void setPosition(Point2f position)
    {
        mPosition = position;
    }
    public boolean isDestroyed()
    {
        return mHit;
    }
    public void setObservers(List<IProjectileObserver> observers) { mObservers = observers; }
    public abstract String getType();
}
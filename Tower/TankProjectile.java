package Tower;

/**
 * @author Jo�o
 * @version 1.0
 * @created 21-mai-2014 11:51:32
 */
public class TankProjectile extends AbstractProjectile
{
    public static final String TYPE = "Tank Projectile";

    @Override
    public String getType()
    {
        return TYPE;
    }
}
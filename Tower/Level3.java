package Tower;

/**
 * @author Jo�o
 * @version 1.0
 * @created 21-mai-2014 11:51:31
 */
public class Level3 implements ILevel
{
    public static final int TYPE = 3;

    @Override
    public int baseCost()
    {
        return 4;
    }

    @Override
    public int damageRate()
    {
        return 4;
    }

    @Override
    public int fireRate()
    {
        return 4;
    }

    @Override
    public void update()
    {
        // TODO
    }

    @Override
    public int getType()
    {
        return TYPE;
    }
}
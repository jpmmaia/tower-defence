package Tower;

/**
 * @author Jo�o
 * @version 1.0
 * @created 21-mai-2014 11:51:31
 */
public class Level1 implements ILevel
{
    public static final int TYPE = 1;

    @Override
    public int baseCost()
    {
        return 2;
    }

    @Override
    public int damageRate()
    {
        return 1;
    }

    @Override
    public int fireRate()
    {
        return 1;
    }

    @Override
    public void update()
    {
        // TODO
    }

    @Override
    public int getType()
    {
        return TYPE;
    }
}
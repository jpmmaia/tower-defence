package Tower;

import Target.IDamageable;

import javax.vecmath.Point2f;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author Jo�o
 * @version 1.0
 * @created 21-mai-2014 11:51:30
 */
public abstract class AbstractTower
{
    protected List<IProjectileObserver> mProjectileObservers = new ArrayList<IProjectileObserver>(1);
    protected AbstractTowerFactory mFactory;
    protected ILevel mLevel;
    protected Point2f mPosition;
    protected int mBaseCost;

    protected int mBaseDamage;
    protected int mDamage;

    protected long mBaseFireRate;
    protected long mFireRate;

    protected long mAccumulatedTicks;
    protected long mLastTick;
    protected List<AbstractProjectile> mProjectiles = new ArrayList<AbstractProjectile>(3);

    public AbstractTower(ILevel level, Point2f position)
    {
        mLevel = level;
        mPosition = position;
    }

    public void update(long deltaMillis, IDamageable target)
    {
        if(target == null)
            return;

        mAccumulatedTicks += deltaMillis * mFireRate;
        while(mAccumulatedTicks > 3000)
        {
            mProjectiles.add(mFactory.createProjectile(mDamage, new Point2f(mPosition), target, mProjectileObservers));
            mAccumulatedTicks -= 3000;
        }

        Iterator<AbstractProjectile> projectile = mProjectiles.iterator();
        while(projectile.hasNext())
        {
            AbstractProjectile p = projectile.next();
            p.update(deltaMillis);
            if(p.isDestroyed())
                projectile.remove();
        }

        mLastTick = deltaMillis;
    }
    public boolean upgrade()
    {
        switch(mLevel.getType())
        {
            case Level1.TYPE:
                setLevel(new Level2());
                break;

            case Level2.TYPE:
                setLevel(new Level3());
                break;

            case Level3.TYPE:
                return false;
        }

        mDamage = mBaseDamage * mLevel.damageRate();
        mFireRate = mBaseFireRate * mLevel.fireRate();

        return true;
    }

    public void addProjectileObserver(IProjectileObserver observer)
    {
        mProjectileObservers.add(observer);
    }
    public int getBaseCost()
    {
        return mBaseCost;
    }
    public int getUpgradeCost()
    {
        return mBaseCost * mLevel.baseCost();
    }
    public int getSellCost()
    {
        return (mBaseCost - 3) * mLevel.baseCost();
    }
    public Point2f getPosition()
    {
        return mPosition;
    }
    public ILevel getLevel()
    {
        return mLevel;
    }
    private void setLevel(ILevel level)
    {
        mLevel = level;
    }
    public void setBaseDamage(int damage)
    {
        mBaseDamage = damage;
        mDamage = mBaseDamage * mLevel.damageRate();
    }
    public int getDamage() { return mDamage; }
    public void setBaseFireRate(long fireRate)
    {
        mBaseFireRate = fireRate;
        mFireRate = mBaseFireRate * mLevel.fireRate();
    }
    public long getFireRate()
    {
        return mFireRate;
    }
    public abstract String getType();
    public boolean isUpgradeable()
    {
        if(mLevel.getType() == Level3.TYPE)
            return false;

        return true;
    }
}
package Tower;

import Target.IDamageable;

import javax.vecmath.Point2f;
import java.util.List;

/**
 * Abstract factory pattern
 */
public abstract class AbstractTowerFactory
{

    /**
     * @param damage
     * @param position
     * @param target
     */
    public abstract AbstractProjectile createProjectile(int damage, Point2f position, IDamageable target, List<IProjectileObserver> observers);

    /**
     * @param level
     */
    public abstract AbstractTower createTower(ILevel level, Point2f position);
}
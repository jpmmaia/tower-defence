package Tower;

/**
 * Created by João on 21/05/2014.
 */
public class NullProjectile extends AbstractProjectile
{
    public static final String TYPE = "Null Projectile";

    @Override
    public String getType()
    {
        return TYPE;
    }
}

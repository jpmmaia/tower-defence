package Tower;

import Target.IDamageable;

import javax.vecmath.Point2f;
import java.util.List;

/**
 * @author Jo�o
 * @version 1.0
 * @created 21-mai-2014 11:51:32
 */
public class TankTowerFactory extends AbstractTowerFactory
{
    static TankTowerFactory mInstance = new TankTowerFactory();

    private TankTowerFactory()
    {
    }

    public static TankTowerFactory getInstance()
    {
        return mInstance;
    }

    @Override
    public AbstractProjectile createProjectile(int damage, Point2f position, IDamageable target, List<IProjectileObserver> observers)
    {
        AbstractProjectile projectile = new TankProjectile();

        projectile.setDamage(damage);
        projectile.setPosition(position);
        projectile.setTarget(target);
        projectile.setObservers(observers);

        return projectile;
    }

    @Override
    public AbstractTower createTower(ILevel level, Point2f position)
    {
        AbstractTower tower = new TankTower(level, position);
        tower.setBaseDamage(5);
        tower.setBaseFireRate(3);

        return tower;
    }
}
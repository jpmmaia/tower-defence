package Tower;

import javax.vecmath.Point2f;

/**
 * @author Jo�o
 * @version 1.0
 * @created 21-mai-2014 11:51:30
 */
public class IceTower extends AbstractTower
{
    public static final String TYPE = "Ice Tower";

    public IceTower(ILevel level, Point2f position)
    {
        super(level, position);
        mFactory = IceTowerFactory.getInstance();
        mBaseCost = 5;
    }

    @Override
    public String getType()
    {
        return TYPE;
    }
}
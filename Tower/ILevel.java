package Tower;

/**
 * @author Jo�o
 * @version 1.0
 * @created 21-mai-2014 11:51:31
 */
public interface ILevel
{
    public int baseCost();

    public int damageRate();

    public int fireRate();

    public void update();

    public int getType();
}
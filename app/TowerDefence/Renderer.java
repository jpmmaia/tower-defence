package app.TowerDefence;

import Game.GameController;
import Game.View.GameSessionView.GameSessionView;
import Game.View.MainMenuView;
import Game.View.SelectionMenuView;
import Models.ShaderHelper;
import android.content.Context;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.opengl.Matrix;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

/**
 * Provides drawing instructions for a GLSurfaceView object. This class must
 * override the OpenGL ES drawing lifecycle methods:
 * <ul>
 * 	<li>{@link android.opengl.GLSurfaceView.Renderer#onSurfaceCreated}</li>
 * 	<li>{@link android.opengl.GLSurfaceView.Renderer#onDrawFrame}</li>
 * 	<li>{@link android.opengl.GLSurfaceView.Renderer#onSurfaceChanged}</li>
 * </ul>
 */
public class Renderer implements GLSurfaceView.Renderer
{
    // mMVPMatrix is an abbreviation for "Model View Projection Matrix"
    private final float[] mMVPMatrix = new float[16];
    private final float[] mProjectionMatrix = new float[16];
    private final float[] mViewMatrix = new float[16];

    protected Context mContext;
    protected GameController mGameController;
    protected long mLastTick;

    /**
     * Renderer constructor
     * @param context
     * @param gameController
     */
    public Renderer(Context context, GameController gameController)
    {
        mContext = context;
        mGameController = gameController;
    }

    /**
     * Called when the surface is created
     * @param unused
     * @param config
     */
    @Override
    public void onSurfaceCreated(GL10 unused, EGLConfig config)
    {
        // Loading required shaders:
        ShaderHelper.loadShaders();

        // Set the background frame color
        GLES20.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

        mLastTick = System.currentTimeMillis();

        // Views need to be instantiated here:
        MainMenuView mainMenuView = new MainMenuView(mContext);
        mGameController.setMainMenuView(mainMenuView);

        SelectionMenuView selectionMenuView = new SelectionMenuView(mContext);
        mGameController.setSelectionMenuView(selectionMenuView);

        GameSessionView gameSessionView = new GameSessionView(mContext);
        mGameController.setGameSessionView(gameSessionView);

        // Start app:
        mGameController.start();
    }

    /**
     * Called every to draw
     * @param unused
     */
    @Override
    public void onDrawFrame(GL10 unused)
    {
        // Draw background color
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);

        // Set the camera position (View matrix)
        Matrix.setLookAtM(mViewMatrix, 0, 0, 0, -3, 0f, 0f, 0f, 0f, 1.0f, 0.0f);

        // Calculate the projection and view transformation
        Matrix.multiplyMM(mMVPMatrix, 0, mProjectionMatrix, 0, mViewMatrix, 0);

        // Update all:
        long currentTick = System.currentTimeMillis();
        mGameController.update(currentTick - mLastTick);
        mLastTick = currentTick;

        // Draw all:
        mGameController.draw(mMVPMatrix);
    }

    /**
     * Called every time the surface changes
     * @param unused
     * @param width
     * @param height
     */
    @Override
    public void onSurfaceChanged(GL10 unused, int width, int height)
    {
        // Adjust the viewport based on geometry changes,
        // such as screen rotation
        GLES20.glViewport(0, 0, width, height);

        // this projection matrix is applied to object coordinates
        // in the onDrawFrame() method
        Matrix.frustumM(mProjectionMatrix, 0, -1, 1, -1, 1, 3, 7);
    }
}
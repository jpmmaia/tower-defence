/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package app.TowerDefence;

import Game.GameController;
import android.content.Context;
import android.opengl.GLSurfaceView;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;

/**
 * A view container where OpenGL ES graphics can be drawn on screen.
 * This view can also be used to capture touch events, such as a user
 * interacting with drawn objects.
 */
public class SurfaceView extends GLSurfaceView
{
    GameController mGameController;

    /**
     * Surface view constructor
     * @param context
     */
    public SurfaceView(Context context)
    {
        super(context);

        // Create an OpenGL ES 2.0 context.
        setEGLContextClientVersion(2);
        setEGLConfigChooser(8, 8, 8, 8, 16, 0);

        mGameController = new GameController(context);

        // Set the Renderer for drawing on the GLSurfaceView
        Renderer renderer = new app.TowerDefence.Renderer(context, mGameController);
        setRenderer(renderer);

        // Render the view only when there is a change in the drawing data
        //setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
    }

    /**
     * Called when the surface view is destroyed
     * @param holder
     */
    @Override
    public void surfaceDestroyed(SurfaceHolder holder)
    {
        mGameController.shutdown();
        System.exit(0);
    }

    /**
     * Called when the user touches the screen
     * @param e
     * @return
     */
    @Override
    public boolean onTouchEvent(MotionEvent e)
    {
        // MotionEvent reports input details from the touch screen
        // and other input controls. In this case, you are only
        // interested in events where the touch position changed.

        switch (e.getAction())
        {
            case MotionEvent.ACTION_DOWN:
                float x = 2.0f * e.getX() / this.getWidth() - 1.0f;
                float y = -2.0f * e.getY() / this.getHeight() + 1.0f;
                Log.println(Log.DEBUG, "Position", String.valueOf(x) + ", " + String.valueOf(y));

                mGameController.touch(x, y);

                requestRender();
        }

        return true;
    }
}

package Models.UserInterface;

import Game.Model.GameManager;
import Terrain.Cell.Cell;

import javax.vecmath.Point2f;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by João on 06/06/2014.
 */
public class TowerStrategy implements IStrategy, IButtonObserver
{
    public static final String TYPE = "Tower Strategy";

    static GameManager mGameManager = GameManager.getInstance();
    Cell mCell;
    Button mUpgrade;
    Button mSell;

    public TowerStrategy(Cell cell)
    {
        mCell = cell;

        float width = cell.getWidth();
        float height = cell.getHeight();
        float xi = cell.getX() - width / 2.0f;
        float y = cell.getY();

        float uYi = y + height / 2.0f;
        mUpgrade = new Button(xi, uYi, width, height);
        mUpgrade.addObserver(this);

        float sYi = y - 3.0f * height / 2.0f;
        mSell = new Button(xi, sYi, width, height);
        mSell.addObserver(this);
    }

    @Override
    public void touch(float x, float y)
    {
        mUpgrade.touch(x, y);
        mSell.touch(x, y);
    }

    @Override
    public List<Point2f> getButtonsPositions()
    {
        List<Point2f> positions = new ArrayList<Point2f>(2);
        positions.add(mUpgrade.getPosition());
        positions.add(mSell.getPosition());
        return positions;
    }

    @Override
    public String getType()
    {
        return TYPE;
    }

    @Override
    public void onTouchEvent(Button button)
    {
        if(button == mUpgrade)
            mGameManager.upgradeTower(mCell);
        else if(button == mSell)
            mGameManager.sellTower(mCell);
    }
}

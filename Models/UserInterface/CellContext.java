package Models.UserInterface;

import Terrain.Cell.Cell;
import Terrain.Cell.EmptyState;
import Terrain.Cell.TowerState;
import Tower.AbstractTower;

import javax.vecmath.Point2f;
import java.util.List;

/**
 * Strategy pattern
 */
public class CellContext
{
    Cell mCell;
    IStrategy mStrategie;
    boolean mEnable = true;

    public CellContext(Cell cell)
    {
        mCell = cell;
        String type = cell.getType();
        if(type == EmptyState.TYPE)
        {
            mStrategie = new EmptyStrategy(cell);
        }
        else if(type == TowerState.TYPE)
        {
            AbstractTower tower = cell.getTower();

            if(tower.isUpgradeable())
                mStrategie = new TowerStrategy(cell);
            else
                mStrategie = new FullUpgradedTowerStrategy(cell);
        }
        else
            mStrategie = new NullStrategy();
    }

    public void touch(float x, float y)
    {
        mStrategie.touch(x, y);

        // Context is disabled after touch command:
        mEnable = false;
        mCell.select(false);
    }

    public boolean isEnabled()
    {
        return mEnable;
    }

    public Point2f getCellPosition()
    {
        return mCell.getPosition();
    }

    public List<Point2f> getButtonsPositions()
    {
        return mStrategie.getButtonsPositions();
    }

    public String getType()
    {
        return mStrategie.getType();
    }
}

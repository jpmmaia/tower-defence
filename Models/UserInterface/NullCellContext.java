package Models.UserInterface;

import Terrain.Cell.Cell;
import Terrain.Cell.NullCell;

/**
 * Created by João on 06/06/2014.
 */
public class NullCellContext extends CellContext
{
    public static final String TYPE = "Null Cell Context";

    public NullCellContext()
    {
        super(new NullCell());
    }

    @Override
    public void touch(float x, float y)
    {
    }

    @Override
    public boolean isEnabled()
    {
        return false;
    }

    @Override
    public String getType()
    {
        return TYPE;
    }
}

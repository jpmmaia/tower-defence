package Models.UserInterface;

import javax.vecmath.Point2f;
import java.util.List;

/**
 * Strategy Pattern
 */
public interface IStrategy
{
    void touch(float x, float y);
    List<Point2f> getButtonsPositions();
    String getType();
}

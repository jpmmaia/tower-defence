package Models.UserInterface;

import javax.vecmath.Point2f;
import java.util.List;

/**
 * Created by João on 06/06/2014.
 */
public class NullStrategy implements IStrategy
{
    public static final String TYPE = "Null Strategy";

    @Override
    public void touch(float x, float y)
    {
    }

    @Override
    public List<Point2f> getButtonsPositions()
    {
        return null;
    }

    @Override
    public String getType()
    {
        return TYPE;
    }
}

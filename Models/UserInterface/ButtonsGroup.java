package Models.UserInterface;

import Game.View.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by João on 05/06/2014.
 */
public class ButtonsGroup
{
    float mX;
    float mY;
    float mWidth;
    float mHeight;
    List<Button> mButtons = new ArrayList<Button>();

    public ButtonsGroup(float x, float y, float width, float height)
    {
        clampDimensions(x, y, width, height);
    }

    public void add(Button button)
    {
        mButtons.add(button);

        revalidateButtons();
    }

    public void remove(Button button)
    {
        mButtons.remove(button);

        revalidateButtons();
    }

    public void setDimensions(float x, float y, float width, float height)
    {
        clampDimensions(x, y, width, height);
        revalidateButtons();
    }

    private void revalidateButtons()
    {
        int size = mButtons.size();
        float bWidth = mWidth / size;
        float bHeight = mHeight;

        for (int i = 0; i < mButtons.size(); i++)
        {
            Button b = mButtons.get(i);
            b.setXi(mX + bWidth * i);
            b.setYi(mY);
            b.setWidth(bWidth);
            b.setHeight(bHeight);
        }
    }

    private void clampDimensions(float x, float y, float width, float height)
    {
        if(x < -1.0f)
            mX = -1.0f;
        else if(x > 1.0f)
            mX = 1.0f - width;
        else
            mX = x;

        if(y < -1.0f)
            mY = -1.0f;
        else if(y > 1.0f)
            mY = 1.0f - height;
        else
            mY = y;

        mWidth = width;
        mHeight = height;
    }
}

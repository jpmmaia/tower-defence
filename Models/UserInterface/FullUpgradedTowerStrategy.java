package Models.UserInterface;

import Game.Model.GameManager;
import Terrain.Cell.Cell;

import javax.vecmath.Point2f;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by João on 06/06/2014.
 */
public class FullUpgradedTowerStrategy implements IStrategy, IButtonObserver
{
    public static final String TYPE = "Full Upgraded Tower Strategy";

    static GameManager mGameManager = GameManager.getInstance();
    Cell mCell;
    Button mSell;

    public FullUpgradedTowerStrategy(Cell cell)
    {
        mCell = cell;

        float width = cell.getWidth();
        float height = cell.getHeight();
        float xi = cell.getX() - width / 2.0f;
        float yi = cell.getY() + height / 2.0f;

        mSell = new Button(xi, yi, width, height);
        mSell.addObserver(this);
    }

    @Override
    public void touch(float x, float y)
    {
        mSell.touch(x, y);
    }

    @Override
    public List<Point2f> getButtonsPositions()
    {
        List<Point2f> positions = new ArrayList<Point2f>(1);
        positions.add(mSell.getPosition());
        return positions;
    }

    @Override
    public String getType()
    {
        return TYPE;
    }

    @Override
    public void onTouchEvent(Button button)
    {
        if(button == mSell)
            mGameManager.sellTower(mCell);
    }
}

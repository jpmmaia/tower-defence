package Models.UserInterface;

import android.content.Context;

/**
 * Created by João on 04/06/2014.
 */
public class NullButton extends Button
{
    public NullButton()
    {
        super(0.0f, 0.0f, 0.0f, 0.0f);
    }

    @Override
    public void touch(float x, float y)
    {
    }

    @Override
    public boolean isTouching(float x, float y)
    {
        return false;
    }

    @Override
    public void addObserver(IButtonObserver observer)
    {
    }

    @Override
    public void notifyObserversOnTouchEvent()
    {
    }
}

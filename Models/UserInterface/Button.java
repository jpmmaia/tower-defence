package Models.UserInterface;

import javax.vecmath.Point2f;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by João on 04/06/2014.
 */
public class Button
{
    float mXi;
    float mYi;
    float mXm;
    float mYm;
    float mXf;
    float mYf;
    boolean mEnabled = true;
    List<IButtonObserver> mObservers = new ArrayList<IButtonObserver>();

    public Button()
    {
    }

    public Button(float x, float y, float width, float height)
    {
        mXi = x;
        mYi = y;
        mXf = x + width;
        mYf = y + height;

        mXm = (mXf + mXi) / 2.0f;
        mYm = (mYf + mYi) / 2.0f;
    }

    public void touch(float x, float y)
    {
        if(!isTouching(x, y))
            return;

        notifyObserversOnTouchEvent();
    }

    public boolean isTouching(float x, float y)
    {
        return mEnabled && x >= mXi && x <= mXf && y >= mYi && y <= mYf;
    }

    public void addObserver(IButtonObserver observer)
    {
        mObservers.add(observer);
    }

    public void notifyObserversOnTouchEvent()
    {
        for (IButtonObserver observer : mObservers)
        {
            observer.onTouchEvent(this);
        }
    }

    public float getXi()
    {
        return mXi;
    }

    public void setXi(float xi)
    {
        mXi = xi;
    }

    public float getYi()
    {
        return mYi;
    }

    public void setYi(float yi)
    {
        mYi = yi;
    }

    public float getXm()
    {
        return mXm;
    }

    public float getYm()
    {
        return mYm;
    }

    public float getWidth()
    {
        return mXf - mXi;
    }

    public void setWidth(float width)
    {
        mXf = mXi + width;
    }

    public float getHeight()
    {
        return mYf - mYi;
    }

    public void setHeight(float height)
    {
        mYf = mYi + height;
    }

    public Point2f getPosition()
    {
        return new Point2f((mXi + mXf) / 2.0f, (mYi + mYf) / 2.0f);
    }

    public void enable(boolean enabled)
    {
        mEnabled = enabled;
    }

    public boolean isEnabled()
    {
        return mEnabled;
    }
}

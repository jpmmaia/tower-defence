package Models.UserInterface;

/**
 * Created by João on 04/06/2014.
 */
public interface IButtonObserver
{
    public void onTouchEvent(Button button);
}

package Models.UserInterface;

import Game.Model.GameManager;
import Terrain.Cell.Cell;
import Tower.AbstractTowerFactory;
import Tower.IceTowerFactory;
import Tower.Level1;
import Tower.TankTowerFactory;
import android.nfc.tech.NfcB;

import javax.vecmath.Point2f;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by João on 05/06/2014.
 * Strategy
 */
public class EmptyStrategy implements IStrategy, IButtonObserver
{
    public static final String TYPE = "Empty Strategy";

    static GameManager mGameManager = GameManager.getInstance();
    Cell mCell;
    Button mCharmanderTower;
    Button mSquirtleTower;

    public EmptyStrategy(Cell cell)
    {
        mCell = cell;

        final int nButtons = 2;
        mCharmanderTower = new Button();
        mCharmanderTower.addObserver(this);
        mSquirtleTower = new Button();
        mSquirtleTower.addObserver(this);


        Point2f position = cell.getPosition();
        float width = cell.getWidth();
        float height = cell.getHeight();
        float gX = position.x - nButtons * width / 2.0f;
        float gY = position.y + height / 2.0f;
        float gWidth = width * nButtons;
        float gHeight = height;

        ButtonsGroup group = new ButtonsGroup(gX, gY, gWidth, gHeight);
        group.add(mCharmanderTower);
        group.add(mSquirtleTower);
    }

    @Override
    public void touch(float x, float y)
    {
        mCharmanderTower.touch(x, y);
        mSquirtleTower.touch(x, y);
    }

    @Override
    public List<Point2f> getButtonsPositions()
    {
        List<Point2f> positions = new ArrayList<Point2f>(3);
        positions.add(mCharmanderTower.getPosition());
        positions.add(mSquirtleTower.getPosition());

        return positions;
    }

    @Override
    public String getType()
    {
        return TYPE;
    }

    @Override
    public void onTouchEvent(Button button)
    {
        if(button == mCharmanderTower)
            mGameManager.createTower(mCell, TankTowerFactory.getInstance());
        else if(button == mSquirtleTower)
            mGameManager.createTower(mCell, IceTowerFactory.getInstance());
    }
}

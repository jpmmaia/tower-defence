package Models;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLES20;
import android.opengl.GLUtils;
import android.util.Log;

import java.nio.IntBuffer;

/**
 * Created by João on 12/05/2014.
 */
public class ShaderHelper
{
    private static final String TAG = "ShaderHelper";
    public static int sp_Image;

    /**
     * Shaders used to render 2D images
     */
    public static final String vs_Image =
                    // Input vertex data, different for all executions of this shader.
                    "#version 300\n" +
                    "layout(location = 0) in vec3 vertexPosition;\n" +
                    "layout(location = 1) in vec2 vertexUV;\n" +

                    // Output data ; will be interpolated for each fragment.
                    "out vec2 UV;\n" +

                    // Uniform data for all vertices
                    "uniform mat4 MVP;\n" +

                    "void main()\n" +
                    "{\n" +
                    "   gl_Position = MVP * vec4(vertexPosition, 1);\n" +
                    "   UV = vertexUV;\n" +
                    "}\n";

    public static final String fs_Image =
                            // Interpolated values from the vertex shaders
                            "#version 300\n" +
                            "precision mediump float;\n" +
                            "in vec2 UV;\n" +

                            // Output data:
                            //"out vec4 color;\n" +

                            // Values that stay constant through the whole mesh:
                            "uniform sampler2D myTextureSampler;\n" +

                            "void main()\n" +
                            "{\n" +
                                // Output color = color of the texture at the specified UV:
                            "   gl_FragColor = texture2D(myTextureSampler, UV).rgba;\n" +
                            "}\n";

    /**
     * Utility method for compiling a OpenGL shader.
     * <p/>
     * <p><strong>Note:</strong> When developing shaders, use the checkGlError()
     * method to debug shader coding errors.</p>
     *
     * @param type       - Vertex or fragment shader type.
     * @param shaderCode - String containing the shader code.
     * @return - Returns an id for the shader.
     */
    public static int loadShader(int type, String shaderCode)
    {
        int shader = GLES20.glCreateShader(type);

        GLES20.glShaderSource(shader, shaderCode);
        GLES20.glCompileShader(shader);

        // Checking for errors
        IntBuffer successBuffer = IntBuffer.allocate(1);
        successBuffer.order();
        successBuffer.put(0);
        successBuffer.position(0);
        GLES20.glGetShaderiv(shader, GLES20.GL_COMPILE_STATUS, successBuffer);
        if (successBuffer.get() == GLES20.GL_FALSE)
        {
            String info = GLES20.glGetShaderInfoLog(shader);
            Log.println(Log.ERROR, "loadShader", info);

            GLES20.glDeleteShader(shader);
        }

        return shader;
    }


    public static final int loadTexture(Context context, int resourceId)
    {
        // Create new texture:
        int[] textures = new int[1];
        GLES20.glGenTextures(1, textures, 0);

        // All future texture functions will modify this texture:
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textures[0]);

        // Load image into OpenGL:
        Bitmap image = BitmapFactory.decodeResource(context.getResources(), resourceId);
        GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, image, 0);

        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR);
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);

        image.recycle();

        return textures[0];
    }

    public static void loadShaders()
    {
        /*int vertexShader = ShaderHelper.loadShader(GLES20.GL_VERTEX_SHADER, ShaderHelper.vs_SolidColor);
        int fragmentShader = ShaderHelper.loadShader(GLES20.GL_FRAGMENT_SHADER, ShaderHelper.fs_SolidColor);

        ShaderHelper.sp_SolidColor = GLES20.glCreateProgram();
        GLES20.glAttachShader(ShaderHelper.sp_SolidColor, vertexShader);
        GLES20.glAttachShader(ShaderHelper.sp_SolidColor, fragmentShader);
        GLES20.glLinkProgram(ShaderHelper.sp_SolidColor);*/


        int vertexShader = ShaderHelper.loadShader(GLES20.GL_VERTEX_SHADER, ShaderHelper.vs_Image);
        int fragmentShader = ShaderHelper.loadShader(GLES20.GL_FRAGMENT_SHADER, ShaderHelper.fs_Image);

        ShaderHelper.sp_Image = GLES20.glCreateProgram();
        GLES20.glAttachShader(ShaderHelper.sp_Image, vertexShader);
        GLES20.glAttachShader(ShaderHelper.sp_Image, fragmentShader);
        GLES20.glLinkProgram(ShaderHelper.sp_Image);

        GLES20.glDetachShader(ShaderHelper.sp_Image, vertexShader);
        GLES20.glDetachShader(ShaderHelper.sp_Image, fragmentShader);
    }

    /**
     * Utility method for debugging OpenGL calls. Provide the name of the call
     * just after making it:
     * <p/>
     * <pre>
     * mColorHandle = GLES20.glGetUniformLocation(mProgram, "vColor");
     * MyGLRenderer.checkGlError("glGetUniformLocation");</pre>
     *
     * If the operation is not successful, the check throws an error.
     *
     * @param glOperation - Name of the OpenGL call to check.
     */
    public static void checkGlError(String glOperation)
    {
        int error;
        while ((error = GLES20.glGetError()) != GLES20.GL_NO_ERROR)
        {
            Log.e(TAG, glOperation + ": glError " + error);
            throw new RuntimeException(glOperation + ": glError " + error);
        }
    }

}

package Models.Textures;

import Terrain.Terrain;
import android.content.Context;
import android.opengl.GLES20;

import javax.vecmath.Point2f;

/**
 * Created by João on 06/06/2014.
 */
public class ProjectileTexture extends GLTexture2D
{
    public ProjectileTexture(Context context, int resourceId)
    {
        super(context, resourceId);
        super.setScale(Terrain.getCellWidth() / 2.0f, Terrain.getCellHeight() / 2.0f, 1.0f);
    }

    public void draw(float[] mvpMatrix, Point2f position)
    {
        super.setPosition(position.x, position.y, 0.03f);
        GLES20.glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);

        GLES20.glEnable(GLES20.GL_BLEND);
        super.draw(mvpMatrix);
        GLES20.glDisable(GLES20.GL_BLEND);
    }
}

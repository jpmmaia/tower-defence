package Models.Textures;

import Terrain.Terrain;
import android.content.Context;
import android.opengl.GLES20;

/**
 * Created by João on 04/06/2014.
 */
public class ButtonTexture extends GLTexture2D
{
    boolean mVisible;

    public ButtonTexture(Context context, int resourceId)
    {
        super(context, resourceId);
        super.setScale(Terrain.getCellWidth(), Terrain.getCellHeight(), 1.0f);
        mVisible = false;
    }

    @Override
    public void draw(float[] mvpMatrix)
    {
        if(!mVisible)
            return;

        GLES20.glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);

        GLES20.glEnable(GLES20.GL_BLEND);
        super.draw(mvpMatrix);
        GLES20.glDisable(GLES20.GL_BLEND);
    }

    public void setPosition(float x, float y)
    {
        super.setPosition(x, y, 0.05f);
    }

    public void setScale(float x, float y)
    {
        super.setScale(x, y, 1.0f);
    }

    @Override
    public void setRotation(float angle)
    {
        super.setRotation(angle);
    }

    public void setVisibility(boolean visible)
    {
        mVisible = visible;
    }
}

package Models.Textures;

import Terrain.Terrain;
import android.content.Context;
import android.opengl.GLES20;
import app.TowerDefence.R;

public class CellCursorTexture extends GLTexture2D
{
    private boolean mVisible = false;

    public CellCursorTexture(Context context)
    {
        super(context, R.drawable.cell_cursor);
        super.setScale(Terrain.getCellWidth(), Terrain.getCellHeight(), 1.0f);
    }

    @Override
    public void draw(float[] mvpMatrix)
    {
        if(!mVisible)
            return;

        GLES20.glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);

        GLES20.glEnable(GLES20.GL_BLEND);
        super.draw(mvpMatrix);
        GLES20.glDisable(GLES20.GL_BLEND);
    }

    @Override
    public void setPosition(float x, float y, float z)
    {
        super.setPosition(x, y, z);
    }

    public void setVisibility(boolean visible)
    {
        mVisible = visible;
    }

    public boolean isVisible()
    {
        return mVisible;
    }
}
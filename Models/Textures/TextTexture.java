package Models.Textures;

import Terrain.Terrain;
import android.content.Context;
import android.opengl.GLES20;

/**
 * Created by João on 07/06/2014.
 */
public class TextTexture extends GLTexture2D
{
    public TextTexture(Context context, int resourceId)
    {
        super(context, resourceId);
    }

    @Override
    public void draw(float[] mvpMatrix)
    {
        GLES20.glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);

        GLES20.glEnable(GLES20.GL_BLEND);
        super.draw(mvpMatrix);
        GLES20.glDisable(GLES20.GL_BLEND);
    }

    public void setPosition(float x, float y, float z)
    {
        super.setPosition(x, y, z);
    }

    public void setScale(float x, float y)
    {
        super.setScale(x, y, 1.0f);
    }
}

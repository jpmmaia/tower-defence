package Models.Textures;

import Target.Monster;
import Terrain.Terrain;
import android.content.Context;
import android.opengl.GLES20;
import app.TowerDefence.R;

import javax.vecmath.Point2f;

/**
 * Created by João on 04/06/2014.
 */
public class MonsterTexture extends GLTexture2D
{
    public MonsterTexture(Context context)
    {
        super(context, R.drawable.pokemon_265);
        super.setScale(Terrain.getCellWidth(), Terrain.getCellHeight(), 1.0f);
    }

    public void draw(float[] mvpMatrix, Point2f position)
    {
        super.setPosition(position.x, position.y, 0.02f);

        GLES20.glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);

        GLES20.glEnable(GLES20.GL_BLEND);
        super.draw(mvpMatrix);
        GLES20.glDisable(GLES20.GL_BLEND);
    }
}

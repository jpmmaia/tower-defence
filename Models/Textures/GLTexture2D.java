/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package Models.Textures;

import Models.ShaderHelper;
import android.content.Context;
import android.opengl.GLES20;
import android.opengl.Matrix;

import javax.vecmath.Point2f;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

/**
 * A two-dimensional square for use as a drawn object in OpenGL ES 2.0.
 *
 * Mother class for all the other texture classes - Template Method Pattern
 */
public abstract class GLTexture2D
{
    private static float[] mVertices;
    private static short[] mIndices;
    private static float[] mTextels;

    private int mVerticesBuffer = -1;
    private int mIndicesBuffer = -1;
    private int mTextelsBuffer = -1;
    protected int mTexture = -1;

    int mMVPHandle;

    private float[] mPosition = { 0.0f, 0.0f, 0.0f };
    private float[] mScale = {1.0f, 1.0f, 1.0f};
    private float mAngle = 0.0f;

    public GLTexture2D()
    {
    }

    /**
     * Sets up the drawing object data for use in an OpenGL ES context.
     */
    public GLTexture2D(Context context, int resourceId)
    {
        initialize(context, resourceId);
    }

    /**
     * Encapsulates the OpenGL ES instructions for drawing this shape.
     *
     * @param mvpMatrix - The Model View Project matrix in which to draw
     *                  this shape.
     */
    protected void draw(final float[] mvpMatrix)
    {
        Matrix.translateM(mvpMatrix, 0, -mPosition[0], mPosition[1], mPosition[2]);
        Matrix.rotateM(mvpMatrix, 0, mAngle, 0.0f, 0.0f, 1.0f);
        Matrix.scaleM(mvpMatrix, 0, mScale[0], mScale[1], mScale[2]);

        GLES20.glUseProgram(ShaderHelper.sp_Image);

        // Enable attributes:
        GLES20.glEnableVertexAttribArray(0);
        GLES20.glEnableVertexAttribArray(1);

        // 1st attribute: vertices
        GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, mVerticesBuffer);
        GLES20.glVertexAttribPointer(0, 3, GLES20.GL_FLOAT, false, 0, 0);

        // 2nd attribute: texture
        GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, mTextelsBuffer);
        GLES20.glVertexAttribPointer(1, 2, GLES20.GL_FLOAT, false, 0, 0);

        // The sampler will use texture unit 0
        int samplerLocation = GLES20.glGetUniformLocation(ShaderHelper.sp_Image, "myTextureSampler");
        GLES20.glUniform1i(samplerLocation, 0);
        //GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mTexture);


        mMVPHandle = GLES20.glGetUniformLocation(ShaderHelper.sp_Image, "MVP");
        GLES20.glUniformMatrix4fv(mMVPHandle, 1, false, mvpMatrix, 0);

        GLES20.glBindBuffer(GLES20.GL_ELEMENT_ARRAY_BUFFER, mIndicesBuffer);
        GLES20.glDrawElements(GLES20.GL_TRIANGLES, mIndices.length, GLES20.GL_UNSIGNED_SHORT, 0);

        // Disabling features:
        GLES20.glBindBuffer(GLES20.GL_ELEMENT_ARRAY_BUFFER, 0);
        GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, 0);
        GLES20.glDisableVertexAttribArray(1);
        GLES20.glDisableVertexAttribArray(0);
    }
    protected void initialize(Context context, int resourceId)
    {
        mTexture = ShaderHelper.loadTexture(context, resourceId);

        mVertices = new float[]
                {
                        -0.5f, -0.5f, 0.0f,
                        0.5f, -0.5f, 0.0f,
                        0.5f, 0.5f, 0.0f,
                        -0.5f, 0.5f, 0.0f,
                };

        mIndices = new short[]
                {
                        0, 1, 2,
                        2, 3, 0,
                };

        mTextels = new float[]
                {
                        1.0f, 1.0f,
                        0.0f, 1.0f,
                        0.0f, 0.0f,
                        1.0f, 0.0f,
                };

        ByteBuffer v = ByteBuffer.allocateDirect(mVertices.length * 4);
        v.order(ByteOrder.nativeOrder());
        FloatBuffer verticesBuffer = v.asFloatBuffer();
        verticesBuffer.put(mVertices);
        verticesBuffer.position(0);

        ByteBuffer i = ByteBuffer.allocateDirect(mIndices.length * 2);
        i.order(ByteOrder.nativeOrder());
        ShortBuffer indicesBuffer = i.asShortBuffer();
        indicesBuffer.put(mIndices);
        indicesBuffer.position(0);

        ByteBuffer t = ByteBuffer.allocateDirect(mTextels.length * 4);
        t.order(ByteOrder.nativeOrder());
        FloatBuffer textelsBuffer = t.asFloatBuffer();
        textelsBuffer.put(mTextels);
        textelsBuffer.position(0);

        // Generate buffers
        int[] buffers = new int[3];
        GLES20.glGenBuffers(3, buffers, 0);

        GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, buffers[0]);
        GLES20.glBufferData(GLES20.GL_ARRAY_BUFFER, verticesBuffer.capacity() * Float.SIZE / 8, verticesBuffer, GLES20.GL_DYNAMIC_DRAW);
        mVerticesBuffer = buffers[0];

        GLES20.glBindBuffer(GLES20.GL_ELEMENT_ARRAY_BUFFER, buffers[1]);
        GLES20.glBufferData(GLES20.GL_ELEMENT_ARRAY_BUFFER, indicesBuffer.capacity() * Short.SIZE / 8, indicesBuffer, GLES20.GL_DYNAMIC_DRAW);
        mIndicesBuffer = buffers[1];

        GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, buffers[2]);
        GLES20.glBufferData(GLES20.GL_ARRAY_BUFFER, textelsBuffer.capacity() * Float.SIZE / 8, textelsBuffer, GLES20.GL_DYNAMIC_DRAW);
        mTextelsBuffer = buffers[2];
    }
    public void shutdown()
    {
        int[] texture = new int[1];
        texture[0] = mTexture;

        GLES20.glDeleteTextures(1, texture, 0);
    }

    protected void setPosition(float x, float y, float z)
    {
        mPosition[0] = x;
        mPosition[1] = y;
        mPosition[2] = z;
    }
    protected void setScale(float x, float y, float z)
    {
        mScale[0] = x;
        mScale[1] = y;
        mScale[2] = z;
    }
    protected void setRotation(float angle)
    {
        mAngle = angle;
    }
}
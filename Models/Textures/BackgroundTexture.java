package Models.Textures;

import android.content.Context;

/**
 * Created by João on 02/06/2014.
 */
public class BackgroundTexture extends GLTexture2D
{
    /**
     * Sets up the drawing object data for use in an OpenGL ES context.
     *
     * @param context
     * @param resourceId
     */
    public BackgroundTexture(Context context, int resourceId)
    {
        super(context, resourceId);

        super.setScale(2.0f, 2.0f, 0.0f);
    }

    @Override
    public void draw(float[] mvpMatrix)
    {
        super.draw(mvpMatrix);
    }
}

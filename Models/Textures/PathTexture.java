package Models.Textures;

import Terrain.Cell.Cell;
import Terrain.Cell.PathState;
import Terrain.Terrain;
import android.content.Context;
import android.opengl.GLES20;
import android.opengl.Matrix;
import app.TowerDefence.R;

import javax.vecmath.Point2f;

/**
 * Created by João on 02/06/2014.
 */
public class PathTexture extends GLTexture2D
{
    /**
     * Sets up the drawing object data for use in an OpenGL ES context.
     *
     * @param context
     */
    public PathTexture(Context context, int resourceId)
    {
        initialize(context, resourceId);

        super.setScale(Terrain.getCellWidth(), Terrain.getCellHeight(), 1.0f);
    }

    @Override
    public void draw(float[] mvpMatrix)
    {
        GLES20.glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);

        GLES20.glEnable(GLES20.GL_BLEND);
        super.draw(mvpMatrix);
        GLES20.glDisable(GLES20.GL_BLEND);
    }

    @Override
    public void setPosition(float x, float y, float z)
    {
        super.setPosition(x, y, z);
    }
}

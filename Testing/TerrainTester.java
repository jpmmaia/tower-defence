package Testing;

import Target.Obstacle;
import Terrain.*;
import Terrain.Cell.*;
import Terrain.TerrainBuilder.TerrainBuilder;
import Terrain.TerrainBuilder.TerrainBuilder1;
import Tower.*;
import org.junit.Assert;
import org.junit.Test;

public class TerrainTester
{
    /**
     * Tests adding cells to the Terrain.
     * tests the selection of the cells (which corresponds to a touch event).
     *
     */
    @Test
    public void testTerrain()
    {
        Terrain terrain = new Terrain();

        // Creating several cells to test:
        Cell cell1 = new Cell();
        Cell cell2 = new Cell();
        Cell cell3 = new Cell();
        Cell cell4 = new Cell();
        Cell cell5 = new Cell();

        // Checking if the insertion was successful:
        Assert.assertTrue(terrain.add(cell1, 0, 0, 1, 1));
        Assert.assertTrue(terrain.add(cell2, 6, 0, 1, 1));
        Assert.assertTrue(terrain.add(cell3, 0, 13, 1, 1));
        Assert.assertTrue(terrain.add(cell4, 6, 13, 1, 1));
        Assert.assertTrue(terrain.add(cell5, 1, 1, 2, 2));

        // Crash Test - insertions which should fail:
        Assert.assertFalse(terrain.add(cell5, 12, 13, 2, 2));
        Assert.assertFalse(terrain.add(cell5, 11, 11, 2, 2));

        // Testing for touch events and corresponding cells being selected, while the other
        //are automatically unselected:
        terrain.select(-0.99f, -0.99f);
        Assert.assertTrue(cell1.isSelected());
        Assert.assertFalse(cell5.isSelected());

        terrain.select(-0.80f, -0.70f);
        Assert.assertFalse(cell1.isSelected());
        Assert.assertTrue(cell5.isSelected());

        terrain.select(-0.99f, 0.76f);
        Assert.assertTrue(cell2.isSelected());

        terrain.select(0.99f, -0.99f);
        Assert.assertTrue(cell3.isSelected());

        terrain.select(0.99f, 0.76f);
        Assert.assertTrue(cell4.isSelected());

        // Crash test - selecting cell where there is none (array out of bound):
        terrain.select(0.99f, 0.99f);
        terrain.select(-0.99f, 0.80f);
    }

    /**
     * Tests the change between states on the cells.
     */
    @Test
    public void testCellStates()
    {
        // Creating game elements:
        Cell cell = new Cell();
        AbstractTower tankTower = TankTowerFactory.getInstance().createTower(new Level1(), cell.getPosition());
        Obstacle obstacle = new Obstacle();

        // Adding tower to the cell and checking corresponding state:
        Assert.assertTrue(cell.addTower(tankTower));
        Assert.assertEquals(cell.getState().getType(), TowerState.TYPE);
        Assert.assertFalse(cell.addTower(tankTower));
        Assert.assertFalse(cell.addObstacle(obstacle));

        // Destroying tower:
        Assert.assertTrue(cell.destroyTower());
        Assert.assertEquals(cell.getState().getType(), EmptyState.TYPE);
        Assert.assertFalse(cell.destroyTower());

        // Adding obstacle:
        Assert.assertTrue(cell.addObstacle(obstacle));
        Assert.assertEquals(cell.getState().getType(), ObstacleState.TYPE);
        Assert.assertFalse(cell.addObstacle(obstacle));
        Assert.assertFalse(cell.destroyTower());

        // Destroying obstacle:
        Assert.assertTrue(cell.destroyObstacle());
        Assert.assertEquals(cell.getState().getType(), EmptyState.TYPE);

        // Path State - must be immutable:
        cell.setState(new PathState(PathState.NULL));

        cell.addTower(tankTower);
        Assert.assertEquals(cell.getState().getType(), PathState.TYPE);

        cell.addObstacle(obstacle);
        Assert.assertEquals(cell.getState().getType(), PathState.TYPE);

        cell.destroyTower();
        Assert.assertEquals(cell.getState().getType(), PathState.TYPE);

        cell.destroyObstacle();
        Assert.assertEquals(cell.getState().getType(), PathState.TYPE);

        // Path cells cannot be selected:
        cell.select(true);
        Assert.assertFalse(cell.isSelected());
    }

    @Test
    public void testBuilder()
    {
        TerrainBuilder builder = new TerrainBuilder1();
        builder.create();

        Terrain terrain = builder.getTerrain();
        CellPath path = builder.getCellPath();
        Cell firstCell = path.getCell(0);
        Cell sixthCell = path.getCell(5);
        Cell ninethCell = path.getCell(8);
        Cell seventeenthCell = path.getCell(16);

        Assert.assertEquals(path, terrain.getPath());

        Assert.assertEquals(PathState.TYPE, firstCell.getType());
        Assert.assertEquals(firstCell, terrain.getCell(1, 2));
        Assert.assertTrue(firstCell.getDirection() == PathState.NORTH);
        Assert.assertEquals(firstCell.getCorner(), PathState.NULL);

        Assert.assertEquals(sixthCell, terrain.getCell(6, 2));
        Assert.assertTrue(sixthCell.getDirection() == PathState.EAST);
        Assert.assertEquals(sixthCell.getCorner(), PathState.TOP_LEFT);

        Assert.assertEquals(ninethCell, terrain.getCell(6, 5));
        Assert.assertTrue(ninethCell.getDirection() == PathState.SOUTH);
        Assert.assertEquals(ninethCell.getCorner(), PathState.TOP_RIGHT);

        Assert.assertEquals(seventeenthCell, terrain.getCell(3, 8));
        Assert.assertTrue(seventeenthCell.getDirection() == PathState.WEST);
        Assert.assertEquals(seventeenthCell.getCorner(), PathState.TOP_RIGHT);
    }
}
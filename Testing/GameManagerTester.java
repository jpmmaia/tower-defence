package Testing;

import Game.Model.GameManager;
import Terrain.Cell.*;
import Tower.*;
import org.junit.Assert;
import org.junit.Test;

public class GameManagerTester
{
    /**
     * Tests the GameManager facade pattern.
     */
    @Test
    public void testCommands()
    {
        GameManager manager = GameManager.getInstance();
        Assert.assertTrue(manager != null);

        // Creating game with empty terrain:
        manager.startLevel(1);
        manager.notifyMoneyChanged(999); // Setting infinite money

        // Simulating touch:
        float x = -0.99f;
        float y = -0.99f;
        manager.touch(x, y);
        Cell selectedCell = manager.getSelectedCell();

        // The selected cell should be the same as the touched:
        Assert.assertSame(manager.getCell(x, y), selectedCell);
        Assert.assertEquals(EmptyState.TYPE, selectedCell.getType());

        // User needs to create a tower:
        manager.createTower(selectedCell, TankTowerFactory.getInstance());
        Assert.assertEquals(TowerState.TYPE, selectedCell.getType());

        // Evolution of the tower and selected cell:
        AbstractTower tower = selectedCell.getTower();
        Assert.assertEquals(TankTower.TYPE, tower.getType());
        Assert.assertEquals(tower.getLevel().getType(), Level1.TYPE);
        Assert.assertTrue(manager.isUpgradeableTower(selectedCell));

        manager.upgradeTower(selectedCell);
        Assert.assertEquals(tower.getLevel().getType(), Level2.TYPE);

        manager.upgradeTower(selectedCell);
        Assert.assertFalse(tower.isUpgradeable());

        // Destroy the tower:
        manager.sellTower(selectedCell);
        Assert.assertEquals(EmptyState.TYPE, selectedCell.getType());
    }
}
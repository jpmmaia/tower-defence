package Testing;

import Target.IDamageable;
import Target.Monster;
import Tower.*;
import org.junit.Assert;
import org.junit.Test;

import javax.vecmath.Point2f;

public class TowerTester
{
    IDamageable target = new Monster();
    Point2f position = new Point2f(0.0f, 0.0f);

    /**
     * Tests the creation of towers and projectiles using the abstract factories.
     */
    @Test
    public void testFactories()
    {
        // Creating towers and a target:
        AbstractTowerFactory tankFactory = TankTowerFactory.getInstance();
        AbstractTowerFactory iceFactory = IceTowerFactory.getInstance();
        IDamageable target = new Monster();

        // Checking if the towers are being created with the correct characteristics:
        AbstractTower tankTower = tankFactory.createTower(new Level1(), new Point2f(0.0f, 0.0f));
        Assert.assertEquals(tankTower.getType(), TankTower.TYPE);

        AbstractProjectile tankProjectile = tankFactory.createProjectile(100, position, target, null);
        Assert.assertEquals(tankProjectile.getType(), TankProjectile.TYPE);

        AbstractTower iceTower = iceFactory.createTower(new Level1(), new Point2f(0.0f, 0.0f));
        Assert.assertEquals(iceTower.getType(), IceTower.TYPE);

        AbstractProjectile iceProjectile = iceFactory.createProjectile(100, position, target, null);
        Assert.assertEquals(iceProjectile.getType(), IceProjectile.TYPE);
    }

    /**
     * Tests how different levels change the power of the towers and projectiles created.
     */
    @Test
    public void testLevels()
    {
        // Creating game elements:
        AbstractTowerFactory tankFactory = TankTowerFactory.getInstance();
        AbstractTower tankTower = tankFactory.createTower(new Level1(), new Point2f(0.0f, 0.0f));
        AbstractProjectile tankProjectile = tankFactory.createProjectile(tankTower.getDamage(), position, target, null);

        // Level 1
        int damage = tankProjectile.getDamage();
        float fireRate = tankTower.getFireRate();
        Assert.assertTrue(damage != 0);
        Assert.assertTrue(fireRate != 0);
        Assert.assertTrue(tankTower.isUpgradeable());
        Assert.assertTrue(tankTower.upgrade());

        // Level 2
        tankProjectile = tankFactory.createProjectile(tankTower.getDamage(), position, target, null);
        Assert.assertEquals(damage * 2, tankProjectile.getDamage());
        Assert.assertEquals(fireRate * 2, tankTower.getFireRate(), 0.01);
        Assert.assertTrue(tankTower.isUpgradeable());
        Assert.assertTrue(tankTower.upgrade());

        // Level 3
        Assert.assertFalse(tankTower.isUpgradeable());
        Assert.assertFalse(tankTower.upgrade());
        tankProjectile = tankFactory.createProjectile(tankTower.getDamage(), position, target, null);
        Assert.assertEquals(damage * 4, tankProjectile.getDamage());
        Assert.assertEquals(fireRate * 4, tankTower.getFireRate(), 0.01);
    }
}
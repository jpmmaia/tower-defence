package Testing;

import Terrain.Terrain;
import Terrain.TerrainBuilder.TerrainBuilder;
import Target.Monster;
import Terrain.Cell.CellPath;
import Terrain.TerrainBuilder.TerrainBuilder1;
import Tower.AbstractProjectile;
import Tower.IProjectileObserver;
import Tower.TankTowerFactory;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.vecmath.Point2f;
import java.util.ArrayList;

/**
 * Created by João on 31/05/2014.
 */
public class MonsterPathTester
{
    static Terrain mTerrain;
    static CellPath mPath;

    @BeforeClass
    public static void initialize()
    {
        TerrainBuilder builder = new TerrainBuilder1();
        builder.create();

        mTerrain = builder.getTerrain();
        mPath = mTerrain.getPath();
        Monster.setPath(mPath);
    }

    @Test
    public void testMove()
    {
        Monster monster = new Monster();
        monster.setVisibility(true);
        Point2f position = monster.getPosition();
        Point2f startPosition = mPath.getStartPoint();

        Assert.assertEquals(startPosition.x, position.x, 0.1);
        Assert.assertEquals(startPosition.y, position.y, 0.1);

        monster.update(2000);
        position = monster.getPosition();

        Point2f cellPosition = mPath.getCell(1).getPosition();
        Assert.assertEquals(cellPosition.x, position.x, 0.1);
        Assert.assertEquals(cellPosition.y, position.y, 0.1);
    }

    @Test
    public void testMoveAndProjectile()
    {
        Monster monster = new Monster(300);
        AbstractProjectile monsterProjectile = TankTowerFactory.getInstance().createProjectile(200, new Point2f(-0.5f, 0.0f), monster, new ArrayList<IProjectileObserver>());

        for(int i = 0; i < 300; i++)
        {
            monster.update(10);
            monsterProjectile.update(10);
        }

        Assert.assertEquals(100, monster.getHealth(), 0.01);
    }
}

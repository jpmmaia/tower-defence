package Testing;

import Target.IDamageable;
import Target.Monster;
import Target.Obstacle;
import Terrain.Cell.NullCellPath;
import Tower.AbstractProjectile;
import Tower.AbstractTowerFactory;
import Tower.IProjectileObserver;
import Tower.TankTowerFactory;

import javax.vecmath.Point2f;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;

/**
 * Created by João on 22/05/2014.
 */
public class TargetTester
{
    /**
     * Tests the damage caused by the projectiles.
     */
    @Test
    public void damageableTest()
    {
        // Create game elements:
        IDamageable monster = new Monster(300);
        IDamageable obstacle = new Obstacle(300);
        AbstractProjectile projectile = TankTowerFactory.getInstance().createProjectile(200, null, null, null);

        // Monster accepts projectile:
        monster.accept(projectile);
        Assert.assertTrue(monster.getHealth() == 100);

        monster.accept(projectile);
        Assert.assertTrue(monster.getHealth() == 0);

        // Obstacle accepts projectile:
        obstacle.accept(projectile);
        Assert.assertTrue(obstacle.getHealth() == 100);

        obstacle.accept(projectile);
        Assert.assertTrue(obstacle.getHealth() == 0);
    }

    /**
     * Tests the collision between the target and the projectile.
     */
    @Test
    public void collisionTest()
    {
        Monster.setPath(new NullCellPath());

        // Creating tank factory:
        AbstractTowerFactory tankFactory = TankTowerFactory.getInstance();

        // Creating monster and projectile whose the monster:
        IDamageable monster = new Monster(300, new Point2f(0.0f, 0.0f));
        AbstractProjectile monsterProjectile = tankFactory.createProjectile(200, new Point2f(-0.5f, 0.0f), monster, new ArrayList<IProjectileObserver>());

        // Creating obstacle and projectile whose target is the obstacle:
        IDamageable obstacle = new Obstacle(300, new Point2f(0.5f, 0.0f));
        AbstractProjectile obstacleProjectile = tankFactory.createProjectile(200, new Point2f(-0.5f, 0.0f), obstacle, new ArrayList<IProjectileObserver>());

        // After 3000 milliseconds, the projectile and targets should have already collided:
        int deltaMillis = 3000;

        // Collision with the monster:
        monster.update(deltaMillis);
        monsterProjectile.update(deltaMillis);
        Assert.assertEquals(100, monster.getHealth());

        // Collision with the obstacle:
        obstacle.update(deltaMillis);
        obstacleProjectile.update(deltaMillis);
        Assert.assertEquals(100, obstacle.getHealth());
    }
}

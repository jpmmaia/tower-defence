package Testing;

import Game.Model.GameManager;
import Models.UserInterface.*;
import Terrain.Cell.Cell;
import Terrain.Cell.EmptyState;
import Terrain.Cell.TowerState;
import Tower.*;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by João on 30/05/2014.
 */
public class UserInterfaceTester
{
    /**
     * Testing buttons touch response.
     */
    @Test
    public void testButtons()
    {
        Button button1 = new Button(0.0f, 0.0f, 0.5f, 0.5f);
        Assert.assertTrue(button1.isEnabled());
        Assert.assertTrue(button1.isTouching(0.2f, 0.2f));
        Assert.assertFalse(button1.isTouching(0.2f, -0.2f));

        // Disabling button:
        button1.enable(false);
        Assert.assertFalse(button1.isTouching(0.2f, 0.2f));
        Assert.assertFalse(button1.isTouching(0.2f, -0.2f));
    }

    /**
     * Testing buttons group. The buttons should automaticaly adjust to the groups position and dimensions.
     */
    @Test
    public void testButtonsGroup()
    {
        Button button1 = new Button();
        Button button2 = new Button();

        ButtonsGroup buttonsGroup = new ButtonsGroup(0.2f, 0.3f, 0.6f, 0.4f);
        buttonsGroup.add(button1);

        Assert.assertEquals(0.2f, button1.getXi(), 0.01f);
        Assert.assertEquals(0.3f, button1.getYi(), 0.01f);
        Assert.assertEquals(0.6f, button1.getWidth(), 0.01f);
        Assert.assertEquals(0.4f, button1.getHeight(), 0.01f);
        Assert.assertTrue(button1.isTouching(0.7f, 0.4f));

        buttonsGroup.add(button2);
        Assert.assertEquals(0.2f, button1.getXi(), 0.01f);
        Assert.assertEquals(0.3f, button1.getYi(), 0.01f);
        Assert.assertEquals(0.3f, button1.getWidth(), 0.01f);
        Assert.assertEquals(0.4f, button1.getHeight(), 0.01f);
        Assert.assertFalse(button1.isTouching(0.7f, 0.4f));

        Assert.assertEquals(0.5f, button2.getXi(), 0.01f);
        Assert.assertEquals(0.3f, button2.getYi(), 0.01f);
        Assert.assertEquals(0.3f, button2.getWidth(), 0.01f);
        Assert.assertEquals(0.4f, button2.getHeight(), 0.01f);
        Assert.assertTrue(button2.isTouching(0.7f, 0.4f));

        // Testing equal dimensions
        Assert.assertEquals(button1.getWidth(), button2.getWidth(), 0.01f);
        Assert.assertEquals(button1.getHeight(), button2.getHeight(), 0.01f);

        // Buttons group must be located in the range x[-1.0f, 1.0f], y[-1.0f, 1.0f]
        ButtonsGroup buttonsGroup2 = new ButtonsGroup(-1.5f, 2.0f, 0.5f, 0.6f);
        Button button3 = new Button();

        buttonsGroup2.add(button3);
        Assert.assertEquals(-1.0f, button3.getXi(), 0.01f);
        Assert.assertEquals(0.4f, button3.getYi(), 0.01f);
    }

    /**
     * Tests the interaction of the touch and the selected cell.
     */
    @Test
    public void testCellContext()
    {
        // Setting infinite money
        GameManager.getInstance().notifyMoneyChanged(999);

        Cell cell = new Cell();
        cell.setX(0.5f);
        cell.setY(0.2f);
        cell.setWidth(0.3f);
        cell.setHeight(0.3f);

        CellContext context = new CellContext(cell);
        Assert.assertEquals(EmptyStrategy.TYPE, context.getType());

        // Disabling context:
        context.touch(-1.0f, -1.0f);
        Assert.assertFalse(context.isEnabled());

        // Touching in the tower build field:
        context = new CellContext(cell);
        context.touch(0.7f, 0.4f);
        Assert.assertFalse(context.isEnabled());
        Assert.assertEquals(TowerState.TYPE, cell.getType());

        AbstractTower tower1 = cell.getTower();
        Assert.assertEquals(IceTower.TYPE, tower1.getType());
        Assert.assertEquals(Level1.TYPE, tower1.getLevel().getType());

        // Upgrading tower:
        context = new CellContext(cell);
        Assert.assertEquals(TowerStrategy.TYPE, context.getType());
        context.touch(0.5f, 0.4f);
        Assert.assertEquals(Level2.TYPE, tower1.getLevel().getType());

        // Upgrading tower to max level:
        context = new CellContext(cell);
        context.touch(0.5f, 0.4f);
        Assert.assertEquals(Level3.TYPE, tower1.getLevel().getType());

        // Sell the tower:
        context = new CellContext(cell);
        Assert.assertEquals(FullUpgradedTowerStrategy.TYPE, context.getType());
        context.touch(0.5f, 0.4f);
        Assert.assertEquals(EmptyState.TYPE, cell.getType());


        // Build a different type of tower:
        context = new CellContext(cell);
        Assert.assertEquals(EmptyStrategy.TYPE, context.getType());
        context.touch(0.3f, 0.4f);
        Assert.assertEquals(TowerState.TYPE, cell.getType());

        AbstractTower tower2 = cell.getTower();
        Assert.assertEquals(TankTower.TYPE, tower2.getType());
        Assert.assertEquals(Level1.TYPE, tower2.getLevel().getType());

        // Sell tower:
        context = new CellContext(cell);
        context.touch(0.5f, 0.0f);
        Assert.assertEquals(EmptyState.TYPE, cell.getType());
    }

    /**
     * Test cell context while interacting through the game manager.
     */
    @Test
    public void testCellContextIntegration()
    {
        GameManager gameManager = GameManager.getInstance();
        gameManager.startLevel(1);

        float x = 0.5f;
        float y = -0.8f;
        Cell cell = gameManager.getCell(x, y);

        // Selecting cell:
        gameManager.touch(x, y);
        Assert.assertTrue(cell.isSelected());
        Assert.assertEquals(EmptyState.TYPE, cell.getType());

        // Building tower:
        gameManager.touch(x + 0.05f, y + cell.getHeight());
        Assert.assertEquals(TowerState.TYPE, cell.getType());
        Assert.assertFalse(cell.isSelected());

        // Selling tower:
        gameManager.touch(x, y);
        gameManager.touch(x, y - cell.getHeight());
        Assert.assertEquals(EmptyState.TYPE, cell.getType());

        gameManager.touch(x, y);
        Assert.assertTrue(cell.isSelected());

        // Testing unfocus:
        Cell cell2 = gameManager.getCell(0.0f, -0.8f);
        Assert.assertFalse(cell2.isSelected());

        gameManager.touch(0.0f, -0.8f);
        Assert.assertFalse(cell.isSelected());
        Assert.assertFalse(cell2.isSelected());
    }
}

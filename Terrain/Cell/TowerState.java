package Terrain.Cell;

import Tower.AbstractProjectile;
import Tower.AbstractTower;

/**
 * Created by João on 05/05/2014.
 */
public class TowerState extends CellState
{
    public static final String TYPE = "Tower State";

    AbstractTower mTower;

    public TowerState(AbstractTower tower)
    {
        mTower = tower;
    }

    @Override
    public AbstractTower getTower()
    {
        return mTower;
    }

    @Override
    public String getType()
    {
        return TYPE;
    }
}

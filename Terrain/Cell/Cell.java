package Terrain.Cell;

import Game.Model.GameManager;
import Target.Obstacle;
import Terrain.Component;
import Tower.AbstractTower;

import javax.vecmath.Point2i;

/**
 * State pattern
 */
public class Cell extends Component
{
    GameManager mGameManager;
    CellState mState;
    int mLine;
    int mCol;

    public Cell()
    {
        super();

        mGameManager = GameManager.getInstance();
        mState = new EmptyState();
    }

    @Override
    public void select(boolean selected)
    {
        if (mState.getType() != PathState.TYPE)
        {
            super.select(selected);
        }
    }

    public boolean addTower(AbstractTower tower)
    {
        if(!isEmpty())
            return false;

        setState(new TowerState(tower));

        return true;
    }

    public boolean destroyTower()
    {
        if(mState.getType() != TowerState.TYPE)
            return false;

        // TODO destroy tower?
        setState(new EmptyState());

        return true;
    }

    public boolean addObstacle(Obstacle obstacle)
    {
        if(!isEmpty())
            return false;

        setState(new ObstacleState(obstacle));

        return true;
    }

    public boolean destroyObstacle()
    {
        if(mState.getType() != ObstacleState.TYPE)
            return false;

        // TODO destroy obstacle?
        setState(new EmptyState());

        return true;
    }

    public CellState getState()
    {
        return mState;
    }

    public void setState(CellState state)
    {
        mState = state;
    }

    public String getType()
    {
        return mState.getType();
    }

    public AbstractTower getTower()
    {
        return mState.getTower();
    }

    public void setDirection(int direction)
    {
        mState.setDirection(direction);
    }

    public int getDirection()
    {
        return mState.getDirection();
    }

    public Obstacle getObstacle()
    {
        return mState.getObstacle();
    }

    public Point2i getGridPosition()
    {
        return new Point2i(mLine, mCol);
    }

    public void corner(int lastDir, int nextDir)
    {
        mState.corner(lastDir, nextDir);
    }

    public int getCorner()
    {
        return mState.getCorner();
    }

    public void setLine(int line)
    {
        mLine = line;
    }

    public void setCol(int col)
    {
        mCol = col;
    }

    @Override
    public boolean isSelected()
    {
        return super.isSelected();
    }

    private boolean isEmpty()
    {
        return mState.getType() == EmptyState.TYPE;
    }
}

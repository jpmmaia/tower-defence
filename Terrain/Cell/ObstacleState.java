package Terrain.Cell;

import Target.Obstacle;
import Tower.AbstractProjectile;
import Tower.AbstractTower;
import Tower.NullProjectile;
import Tower.NullTower;

/**
 * Created by João on 05/05/2014.
 */
public class ObstacleState extends CellState
{
    public static final String TYPE = "Obstacle State";

    Obstacle mObstacle;

    public ObstacleState(Obstacle obstacle)
    {
        mObstacle = obstacle;
    }

    @Override
    public Obstacle getObstacle()
    {
        return mObstacle;
    }

    @Override
    public String getType()
    {
        return TYPE;
    }
}

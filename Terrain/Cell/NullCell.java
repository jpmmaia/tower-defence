package Terrain.Cell;

import javax.vecmath.Point2f;

/**
 * Created by João on 22/05/2014.
 */
public class NullCell extends Cell
{
    public static final String TYPE = "Null Cell";

    public NullCell()
    {
    }

    @Override
    public void select(boolean selected)
    {
    }

    @Override
    public CellState getState()
    {
        return new EmptyState();
    }

    @Override
    public void setState(CellState state)
    {
    }

    @Override
    public String getType()
    {
        return TYPE;
    }

    @Override
    public Point2f getPosition()
    {
        return new Point2f(0.0f, 0.0f);
    }

    @Override
    public boolean isSelected()
    {
        return false;
    }
}

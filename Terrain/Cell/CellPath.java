package Terrain.Cell;

import javax.vecmath.Point2f;
import javax.vecmath.Vector2f;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by João on 31/05/2014.
 */
public class CellPath
{
    ArrayList<Cell> mPath = new ArrayList<Cell>();

    public void addCell(Cell cell, int direction)
    {
        cell.setState(new PathState(direction));
        mPath.add(cell);
    }

    public Cell getCell(int index)
    {
        return mPath.get(index);
    }

    public Point2f getPosition(float distance)
    {
        // Calculating percentage between one cell and the next:
        int index = (int)Math.floor(distance);
        float between = distance - index;

        // Calculating direction:
        Point2f currentCellPosition = mPath.get(index).getPosition();
        Point2f nextCellPosition = mPath.get(index + 1).getPosition();
        Vector2f direction = new Vector2f(nextCellPosition.x, nextCellPosition.y);
        direction.sub(currentCellPosition);
        direction.scale(between);

        // Calculating final position:
        currentCellPosition.add(direction);

        return currentCellPosition;
    }

    public Point2f getStartPoint()
    {
        return mPath.get(0).getPosition();
    }

    public boolean isAtEnd(float distance)
    {
        return distance >= mPath.size() - 1;
    }

    public Cell getFirstCell()
    {
        return mPath.get(0);
    }
    public Cell getLastCell()
    {
        return mPath.get(mPath.size() - 1);
    }

    public Iterator<Cell> getIterator()
    {
        return mPath.iterator();
    }

    public int getSize()
    {
        return mPath.size();
    }
}

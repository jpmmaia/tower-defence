package Terrain.Cell;

import Models.Textures.PathTexture;
import Terrain.Terrain;
import Tower.AbstractProjectile;
import Tower.AbstractTower;
import Tower.NullProjectile;
import Tower.NullTower;

/**
 * Created by João on 05/05/2014.
 */
public class PathState extends CellState
{
    public static final String TYPE = "Path State";
    public static final int NULL = 0;
    public static final int EAST = 1;
    public static final int NORTH = 2;
    public static final int WEST = 4;
    public static final int SOUTH = 8;

    public static final int TOP_LEFT = 1;
    public static final int TOP_RIGHT = 2;
    public static final int BOTTOM_RIGHT = 3;
    public static final int BOTTOM_LEFT = 4;

    int mDirection;
    int mCorner = NULL;

    public PathState(int direction)
    {
        mDirection = direction;
    }

    @Override
    public int getCorner()
    {
        return mCorner;
    }

    @Override
    public void corner(int lastDir, int nextDir)
    {
        if(lastDir == NORTH)
            mCorner = nextDir == EAST ? TOP_LEFT : TOP_RIGHT;

        else if(lastDir == SOUTH)
            mCorner = nextDir == EAST ? BOTTOM_LEFT : BOTTOM_RIGHT;

        else if(lastDir == EAST)
            mCorner = nextDir == NORTH ? BOTTOM_RIGHT : TOP_RIGHT;

        else if(lastDir == WEST)
            mCorner = nextDir == NORTH ? BOTTOM_LEFT : TOP_LEFT;
    }

    @Override
    public void setDirection(int direction)
    {
        mDirection = direction;
    }

    @Override
    public int getDirection()
    {
        return mDirection;
    }

    @Override
    public String getType()
    {
        return TYPE;
    }
}

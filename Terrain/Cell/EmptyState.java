package Terrain.Cell;

import Tower.AbstractProjectile;
import Tower.AbstractTower;
import Tower.NullProjectile;
import Tower.NullTower;

/**
 * Created by João on 05/05/2014.
 */
public class EmptyState extends CellState
{
    public static final String TYPE = "Empty State";

    @Override
    public String getType()
    {
        return TYPE;
    }
}

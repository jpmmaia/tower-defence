package Terrain.Cell;

import Target.Obstacle;
import Tower.AbstractProjectile;
import Tower.AbstractTower;
import Tower.NullProjectile;
import Tower.NullTower;
import android.graphics.Path;

/**
 * Created by João on 05/05/2014.
 */
public abstract class CellState
{
    void update()
    {
    }

    public int getCorner()
    {
        return PathState.NULL;
    }

    public void corner(int lastDir, int nextDir)
    {
    }

    public void setDirection(int direction)
    {
    }

    public int getDirection()
    {
        return PathState.NULL;
    }

    public Obstacle getObstacle()
    {
        return null;
    }

    public AbstractTower getTower()
    {
        return new NullTower();
    }

    public abstract String getType();
}

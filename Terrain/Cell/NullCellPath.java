package Terrain.Cell;

import javax.vecmath.Point2f;

/**
 * Created by João on 02/06/2014.
 */
public class NullCellPath extends CellPath
{
    @Override
    public Point2f getStartPoint()
    {
        return new Point2f(0.0f, 0.0f);
    }

    @Override
    public void addCell(Cell cell, int direction)
    {
    }

    @Override
    public Cell getCell(int index)
    {
        return new NullCell();
    }

    @Override
    public Point2f getPosition(float distance)
    {
        return new Point2f(0.0f, 0.0f);
    }
}

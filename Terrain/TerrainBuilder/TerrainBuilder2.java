package Terrain.TerrainBuilder;

import Terrain.Cell.PathState;

import javax.vecmath.Point2i;

/**
 * Created by João on 08/06/2014.
 */
public class TerrainBuilder2 extends TerrainBuilder
{
    @Override
    public void create()
    {
        setStartPoint(new Point2i(3, 6));
        addPath(PathState.SOUTH, 1);
        addPath(PathState.WEST, 5);
        addPath(PathState.NORTH, 4);
        addPath(PathState.EAST, 11);
        addPath(PathState.SOUTH, 4);
        addPath(PathState.WEST, 4);
        addPath(PathState.NORTH, 2);
        addPath(PathState.EAST, 3);
    }
}

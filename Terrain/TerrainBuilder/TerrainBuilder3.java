package Terrain.TerrainBuilder;

import Terrain.Cell.PathState;

import javax.vecmath.Point2i;

/**
 * Created by João on 08/06/2014.
 */
public class TerrainBuilder3 extends TerrainBuilder
{
    @Override
    public void create()
    {
        setStartPoint(new Point2i(1, 13));
        addPath(PathState.NORTH, 1);
        addPath(PathState.WEST, 2);
        addPath(PathState.SOUTH, 1);
        addPath(PathState.WEST, 2);
        addPath(PathState.NORTH, 3);
        addPath(PathState.EAST, 4);
        addPath(PathState.NORTH, 1);
        addPath(PathState.WEST, 6);
        addPath(PathState.SOUTH, 4);
        addPath(PathState.WEST, 2);
        addPath(PathState.NORTH, 4);
        addPath(PathState.WEST, 4);
        addPath(PathState.SOUTH, 5);
    }
}

package Terrain.TerrainBuilder;

import Terrain.Cell.PathState;

import javax.vecmath.Point2i;

/**
 * Created by João on 08/06/2014.
 */
public class TerrainBuilder1 extends TerrainBuilder
{
    @Override
    public void create()
    {
        setStartPoint(new Point2i(1, 2));
        addPath(PathState.NORTH, 5);
        addPath(PathState.EAST, 3);
        addPath(PathState.SOUTH, 4);
        addPath(PathState.EAST, 3);
        addPath(PathState.NORTH, 1);
        addPath(PathState.WEST, 1);
        addPath(PathState.NORTH, 2);
        addPath(PathState.EAST, 4);
        addPath(PathState.SOUTH, 4);
    }
}

package Terrain.TerrainBuilder;

import Terrain.Cell.Cell;
import Terrain.Cell.PathState;
import Terrain.Cell.CellPath;
import Terrain.Terrain;
import android.content.Context;

import javax.vecmath.Point2f;
import javax.vecmath.Point2i;

/**
 * Created by João on 23/05/2014.
 */
public abstract class TerrainBuilder
{
    Terrain mTerrain;
    CellPath mPath;

    public TerrainBuilder()
    {
        mTerrain = new Terrain();
        mPath = mTerrain.getPath();
    }

    public abstract void create();

    public Terrain getTerrain()
    {
        return mTerrain;
    }

    public CellPath getCellPath()
    {
        return mPath;
    }

    public void setStartPoint(Point2i startPoint)
    {
        mPath.addCell(mTerrain.getCell(startPoint.x, startPoint.y), PathState.NULL);
    }

    public void addPath(int direction, int length)
    {
        int line, col, sign = 1;

        Cell lastCell = mPath.getLastCell();
        int lastDirection = lastCell.getDirection();
        lastCell.corner(lastDirection, direction);

        lastCell.setDirection(direction);
        Point2i lastGridPosition = lastCell.getGridPosition();

        switch(direction)
        {
            case PathState.WEST:
                sign = -1;
            case PathState.EAST:
                line = lastGridPosition.x;
                for(col = 1; col <= length; col++)
                    mPath.addCell(mTerrain.getCell(line, lastGridPosition.y + col * sign), direction);
                break;

            case PathState.SOUTH:
                sign = -1;
            case PathState.NORTH:
                col = lastGridPosition.y;
                for(line = 1; line <= length; line++)
                    mPath.addCell(mTerrain.getCell(lastGridPosition.x + line * sign, col), direction);

                break;
        }
    }
}

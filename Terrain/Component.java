package Terrain;

import javax.vecmath.Point2f;

/**
 * Created by João on 05/05/2014.
 */
public class Component
{
    private static Component mCurrentSelected = null;

    protected float mX;
    protected float mY;
    protected float mWidth;
    protected float mHeight;

    private boolean mSelected = false;

    public Component()
    {
        mX = 0.0f;
        mY = 0.0f;
        mWidth = 0.0f;
        mHeight = 0.0f;
    }

    public Component(float x, float y, float width, float height)
    {
        mX = x;
        mY = y;
        mWidth = width;
        mHeight = height;
    }

    protected void select(boolean selected)
    {
        mSelected = selected;
    }

    public float getX()
    {
        return mX;
    }

    public void setX(float x)
    {
        mX = x;
    }

    public float getY()
    {
        return mY;
    }

    public void setY(float y)
    {
        mY = y;
    }

    public Point2f getPosition()
    {
        return new Point2f(mX, mY);
    }

    public float getWidth()
    {
        return mWidth;
    }

    public void setWidth(float width)
    {
        mWidth = width;
    }

    public float getHeight()
    {
        return mHeight;
    }

    public void setHeight(float height)
    {
        mHeight = height;
    }

    protected boolean isSelected()
    {
        return mSelected;
    }
}

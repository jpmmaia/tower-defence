package Terrain;

import Terrain.Cell.Cell;
import Terrain.Cell.CellPath;
import Terrain.Cell.NullCell;

import javax.vecmath.Point2i;
import java.util.ArrayList;

/**
 * Created by João on 05/05/2014.
 */
public class Terrain
{
    static final float mRatio = 16.0f / 9.0f;
    static final int mCols = 14;
    static final int mLines = 7;
    static float mX = -1.0f;
    static float mY = -1.0f;
    static float mWidth = 2.0f;
    static float mHeight = 1.78f;
    static float mCellWidth = mWidth / mCols;
    static float mCellHeight = mHeight / mLines;

    Cell[][] mMap;
    CellPath mPath = new CellPath();
    Cell mSelected = new NullCell();

    public Terrain()
    {
        super();
        initialize();
    }

    private void initialize()
    {
        mMap = new Cell[mLines][mCols];
        for (int i = 0; i < mMap.length; i++)
        {
            for (int j = 0; j < mMap[i].length; j++)
            {
                this.add(new Cell(), i, j, 1, 1);
            }
        }
    }

    public boolean add(Cell cell, int line, int col, int width, int height)
    {
        if (line < 0 || col < 0 || line + width > mLines || col + height > mCols)
            return false;

        for (int l = 0; l < height; l++)
            for (int c = 0; c < width; c++)
                mMap[line + l][col + c] = cell;


        cell.setX(col * mCellWidth - 1.0f + mCellWidth / 2.0f);
        cell.setY(line * mCellHeight - 1.0f + mCellHeight / 2.0f);
        cell.setWidth(width * mCellWidth);
        cell.setHeight(height * mCellHeight);
        cell.setLine(line);
        cell.setCol(col);

        return true;
    }

    public Cell select(float x, float y)
    {
        Cell selected = getCell(x, y);

        mSelected.select(false);
        mSelected = selected;
        mSelected.select(true);

        return mSelected;
    }

    public Cell getSelected()
    {
        return mSelected;
    }

    public Cell getCell(float x, float y)
    {
        int col = Math.round((float) Math.floor((x + 1.0f) / mCellWidth));
        int line = Math.round((float) Math.floor((y + 1.0f) / mCellHeight));

        if (line < 0 || col < 0 || line >= mLines || col >= mCols)
            return new NullCell();

        return mMap[line][col];
    }

    public Cell getCell(int line, int col)
    {
        return line < 0 || line >= mLines || col < 0 || col >= mCols ? new NullCell() : mMap[line][col];
    }

    public CellPath getPath()
    {
        return mPath;
    }

    public static float getCellWidth()
    {
        return mCellWidth;
    }

    public static float getCellHeight()
    {
        return mCellHeight;
    }
}

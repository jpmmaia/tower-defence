package Game.View;

import Game.Model.Menu.IMenu;
import Models.Textures.BackgroundTexture;
import Models.Textures.ButtonTexture;
import Models.UserInterface.Button;
import android.content.Context;
import android.media.MediaPlayer;
import app.TowerDefence.R;

import java.util.List;

/**
 * Created by João on 07/06/2014.
 */
public class MainMenuView implements IView
{
    BackgroundTexture mBackground;
    ButtonTexture mStartButton;

    public MainMenuView(Context context)
    {
        mBackground = new BackgroundTexture(context, R.drawable.pokemon_start_menu);

        mStartButton = new ButtonTexture(context, R.drawable.button_start);
        mStartButton.setVisibility(true);
    }
    public void initialize(IMenu menu)
    {
        List<Button> buttons = menu.getButtons();
        Button startButton = buttons.get(0);

        float width = startButton.getWidth();
        float height = startButton.getHeight();

        mStartButton.setPosition(startButton.getXm(), startButton.getYm());
        mStartButton.setScale(width, height);
    }
    @Override
    public void shutdown()
    {
        mBackground.shutdown();
        mStartButton.shutdown();
    }

    @Override
    public void draw(float[] mvpMatrix)
    {
        mBackground.draw(mvpMatrix.clone());
        mStartButton.draw(mvpMatrix.clone());
    }
}
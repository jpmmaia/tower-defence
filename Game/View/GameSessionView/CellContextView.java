package Game.View.GameSessionView;

import Game.View.IView;
import Models.Textures.ButtonTexture;
import Models.Textures.CellCursorTexture;
import Models.UserInterface.*;
import Terrain.Cell.Cell;
import android.content.Context;
import app.TowerDefence.R;

import javax.vecmath.Point2f;
import java.util.List;

/**
 * Created by João on 07/06/2014.
 */
public class CellContextView implements IView
{
    CellCursorTexture mCellCursorTexture;
    ButtonTexture mCharmanderButton;
    ButtonTexture mSquirtleButton;
    ButtonTexture mDollarButton;
    ButtonTexture mArrowButton;

    public CellContextView(Context context)
    {
        mCellCursorTexture = new CellCursorTexture(context);
        mCharmanderButton = new ButtonTexture(context, R.drawable.pokemon_004);
        mSquirtleButton = new ButtonTexture(context, R.drawable.pokemon_007);
        mDollarButton = new ButtonTexture(context, R.drawable.dollar_glow);
        mArrowButton = new ButtonTexture(context, R.drawable.arrow_glow);
    }
    public void initialize()
    {
        mCellCursorTexture.setVisibility(false);
        mCharmanderButton.setVisibility(false);
        mSquirtleButton.setVisibility(false);
        mDollarButton.setVisibility(false);
        mArrowButton.setVisibility(false);
    }
    @Override
    public void shutdown()
    {
        mCellCursorTexture.shutdown();
        mCellCursorTexture.shutdown();
        mSquirtleButton.shutdown();
        mDollarButton.shutdown();
        mArrowButton.shutdown();
    }

    public void draw(float[] mvpMatrix)
    {
        mCellCursorTexture.draw(mvpMatrix.clone());
        mCharmanderButton.draw(mvpMatrix.clone());
        mSquirtleButton.draw(mvpMatrix.clone());
        mDollarButton.draw(mvpMatrix.clone());
        mArrowButton.draw(mvpMatrix.clone());
    }

    public void cellSelected(CellContext cellContext, Cell cell)
    {
        if(cellContext.getType() == NullStrategy.TYPE)
            return;

        Point2f position = cellContext.getCellPosition();
        mCellCursorTexture.setPosition(position.x, position.y, 0.01f);
        mCellCursorTexture.setVisibility(true);

        List<Point2f> buttonsPositions = cellContext.getButtonsPositions();
        String type = cellContext.getType();
        if(type == EmptyStrategy.TYPE)
        {
            Point2f charmanderPosition = buttonsPositions.get(0);
            mCharmanderButton.setPosition(charmanderPosition.x, charmanderPosition.y);
            mCharmanderButton.setVisibility(true);

            Point2f squirtlePosition = buttonsPositions.get(1);
            mSquirtleButton.setPosition(squirtlePosition.x, squirtlePosition.y);
            mSquirtleButton.setVisibility(true);
        }
        else if(type == TowerStrategy.TYPE)
        {
            Point2f upgradePosition = buttonsPositions.get(0);
            mArrowButton.setPosition(upgradePosition.x, upgradePosition.y);
            mArrowButton.setVisibility(true);

            Point2f sellPosition = buttonsPositions.get(1);
            mDollarButton.setPosition(sellPosition.x, sellPosition.y);
            mDollarButton.setVisibility(true);
        }
        else if(type == FullUpgradedTowerStrategy.TYPE)
        {
            Point2f dollarPosition = buttonsPositions.get(0);
            mDollarButton.setPosition(dollarPosition.x, dollarPosition.y);
            mDollarButton.setVisibility(true);
        }
    }

    public void cellUnselected()
    {
        mCellCursorTexture.setVisibility(false);
        mCharmanderButton.setVisibility(false);
        mSquirtleButton.setVisibility(false);
        mDollarButton.setVisibility(false);
        mArrowButton.setVisibility(false);
    }
}

package Game.View.GameSessionView;

import Game.Model.GameSession.GameSession;
import Game.Model.GameSession.IGameSessionObserver;
import Game.View.*;
import Models.Textures.*;
import Models.UserInterface.*;
import Terrain.Cell.Cell;
import Tower.AbstractProjectile;
import Tower.AbstractTower;
import android.content.Context;
import app.TowerDefence.R;

import javax.vecmath.Point2f;

/**
 * Created by João on 02/06/2014.
 */
public class GameSessionView implements IView, IGameSessionObserver
{
    BackgroundTexture mBackground;

    TowersView mTowersView;
    PathView mPathView;
    ToolBarView mToolBarView;
    CellContextView mCellContextView;
    MonstersView mMonstersView;
    GameStatusView mGameStatusView;

    public GameSessionView(Context context)
    {
        mTowersView = new TowersView(context);
        mToolBarView = new ToolBarView(context);
        mPathView = new PathView(context);
        mCellContextView = new CellContextView(context);
        mMonstersView = new MonstersView(context);
        mGameStatusView = new GameStatusView(context);

        mBackground = new BackgroundTexture(context, R.drawable.background1);
    }
    public void initialize(GameSession gameSession)
    {
        mTowersView.initialize();
        mToolBarView.initialize(gameSession.getTotalWaves());
        mPathView.initialize(gameSession.getPath());
        mCellContextView.initialize();
        mMonstersView.initialize();
        mGameStatusView.initialize();
    }
    @Override
    public void shutdown()
    {
        mTowersView.shutdown();
        mToolBarView.shutdown();
        mPathView.shutdown();
        mCellContextView.shutdown();
        mMonstersView.shutdown();
        mBackground.shutdown();
    }


    public void draw(float[] mvpMatrix)
    {
        mBackground.draw(mvpMatrix.clone());
        mToolBarView.draw(mvpMatrix);
        mPathView.draw(mvpMatrix);

        mMonstersView.draw(mvpMatrix);
        mTowersView.draw(mvpMatrix);
        mCellContextView.draw(mvpMatrix);
        mGameStatusView.draw(mvpMatrix);
    }

    @Override
    public void moneyChanged(int money)
    {
        mToolBarView.setMoney(money);
    }

    @Override
    public void gameEnded(boolean victory)
    {
        mGameStatusView.setStatus(victory);
    }

    @Override
    public void monsterUpdated(Point2f position)
    {
        mMonstersView.monsterUpdated(position);
    }
    @Override
    public void towerCreated(AbstractTower tower)
    {
        mTowersView.towerCreated(tower);
    }
    @Override
    public void towerUpgraded(AbstractTower tower)
    {
        mTowersView.towerUpgraded(tower);
    }
    @Override
    public void towerDestroyed(AbstractTower tower)
    {
        mTowersView.towerDestroyed(tower);
    }
    @Override
    public void projectileUpdated(AbstractProjectile projectile, String type)
    {
        mTowersView.projectileUpdated(projectile, type);
    }

    @Override
    public void newSpawnWave(int currentWave)
    {
        mToolBarView.setCurrentWave(currentWave);
    }
    @Override
    public void livesChanged(int lives)
    {
        mToolBarView.setLives(lives);
    }

    @Override
    public void cellSelected(CellContext cellContext, Cell cell)
    {
        mCellContextView.cellSelected(cellContext, cell);
    }
    @Override
    public void cellUnselected()
    {
        mCellContextView.cellUnselected();
    }
}

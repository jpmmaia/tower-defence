package Game.View.GameSessionView;

import Game.View.IView;
import Models.Textures.BarTexture;
import Models.Textures.TextTexture;
import Terrain.Terrain;
import android.content.Context;
import app.TowerDefence.R;

/**
 * Created by João on 07/06/2014.
 */
public class ToolBarView implements IView
{
    BarTexture mToolBar;

    BarTexture mPokeball;
    NumbersView mMoneyView;

    NumbersView mCurrentWaveView;
    TextTexture mSlash;
    NumbersView mTotalWavesView;

    BarTexture mHeart;
    NumbersView mLivesView;

    public ToolBarView(Context context)
    {
        float z = 0.02f;

        mToolBar = new BarTexture(context, R.drawable.bar);
        mToolBar.setPosition(0.0f, 0.90f, z - 0.01f);
        mToolBar.setScale(2.0f, 0.2f);

        mPokeball = new BarTexture(context, R.drawable.pokeball);
        mPokeball.setPosition(-0.95f, 0.91f, 0.02f);
        mMoneyView = new NumbersView(context, -0.87f, 0.9f, z);

        mCurrentWaveView = new NumbersView(context, -0.05f, 0.9f, z);
        mSlash = new TextTexture(context, R.drawable.slash);
        mSlash.setScale(Terrain.getCellWidth() / 2.0f, Terrain.getCellHeight() / 2.0f);
        mSlash.setPosition(0.0f, 0.9f, z);
        mTotalWavesView = new NumbersView(context, 0.05f, 0.9f, z);

        mHeart = new BarTexture(context, R.drawable.heart);
        mHeart.setPosition(0.82f, 0.9f, z);
        mLivesView = new NumbersView(context, 0.9f, 0.9f, z);
    }
    public void initialize(int totalWaves)
    {
        mMoneyView.initialize();
        mCurrentWaveView.initialize();
        mTotalWavesView.initialize();
        mLivesView.initialize();

        mTotalWavesView.setNumber(totalWaves);
    }
    @Override
    public void shutdown()
    {
        mToolBar.shutdown();
        mPokeball.shutdown();
        mMoneyView.shutdown();
        mCurrentWaveView.shutdown();
        mSlash.shutdown();
        mTotalWavesView.shutdown();
        mHeart.shutdown();
        mLivesView.shutdown();
    }

    public void draw(float[] mvpMatrix)
    {
        mToolBar.draw(mvpMatrix.clone());

        mPokeball.draw(mvpMatrix.clone());
        mMoneyView.draw(mvpMatrix);

        mCurrentWaveView.draw(mvpMatrix);
        mSlash.draw(mvpMatrix.clone());
        mTotalWavesView.draw(mvpMatrix);

        mHeart.draw(mvpMatrix.clone());
        mLivesView.draw(mvpMatrix);
    }

    public void setMoney(int money)
    {
        mMoneyView.setNumber(money);
    }
    public void setCurrentWave(int currentWave)
    {
        mCurrentWaveView.setNumber(currentWave);
    }
    public void setTotalWaves(int totalWaves)
    {
        mTotalWavesView.setNumber(totalWaves);
    }
    public void setLives(int lives)
    {
        mLivesView.setNumber(lives);
    }
}

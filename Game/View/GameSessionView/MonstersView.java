package Game.View.GameSessionView;

import Game.View.IView;
import Models.Textures.MonsterTexture;
import android.content.Context;

import javax.vecmath.Point2f;
import java.util.LinkedList;
import java.util.Queue;

/**
 * Created by João on 07/06/2014.
 */
public class MonstersView implements IView
{
    MonsterTexture mMonsterTexture;
    Queue<Point2f> mMonstersQueue;

    public MonstersView(Context context)
    {
        mMonsterTexture = new MonsterTexture(context);
    }
    public void initialize()
    {
        mMonstersQueue = new LinkedList<Point2f>();
    }
    @Override
    public void shutdown()
    {
        mMonsterTexture.shutdown();
    }

    public void draw(float[] mvpMatrix)
    {
        while(!mMonstersQueue.isEmpty())
            mMonsterTexture.draw(mvpMatrix.clone(), mMonstersQueue.remove());
    }

    public void monsterUpdated(Point2f position)
    {
        mMonstersQueue.add(position);
    }
}

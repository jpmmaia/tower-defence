package Game.View.GameSessionView;

import Game.View.IView;
import Models.Textures.ProjectileTexture;
import Models.Textures.TowerTexture;
import Tower.AbstractProjectile;
import Tower.AbstractTower;
import Tower.TankProjectile;
import Tower.TankTower;
import android.content.Context;
import app.TowerDefence.R;

import javax.vecmath.Point2f;
import java.util.*;

/**
 * Created by João on 07/06/2014.
 */
public class TowersView implements IView
{
    Map<AbstractTower, Integer> mTowers;
    List<TowerTexture> mTowerTextures;

    ProjectileTexture mFireTexture;
    ProjectileTexture mWaterTexture;
    Queue<Point2f> mFirePositions;
    Queue<Point2f> mWaterPositions;

    public TowersView(Context context)
    {
        mTowerTextures = new ArrayList<TowerTexture>();
        mTowerTextures.add(new TowerTexture(context, R.drawable.pokemon_004));
        mTowerTextures.add(new TowerTexture(context, R.drawable.pokemon_005));
        mTowerTextures.add(new TowerTexture(context, R.drawable.pokemon_006));
        mTowerTextures.add(new TowerTexture(context, R.drawable.pokemon_007));
        mTowerTextures.add(new TowerTexture(context, R.drawable.pokemon_008));
        mTowerTextures.add(new TowerTexture(context, R.drawable.pokemon_009));

        mFireTexture = new ProjectileTexture(context, R.drawable.fire_ball);
        mWaterTexture = new ProjectileTexture(context, R.drawable.water_wind);
    }
    public void initialize()
    {
        mTowers = new Hashtable<AbstractTower, Integer>();
        mFirePositions = new LinkedList<Point2f>();
        mWaterPositions = new LinkedList<Point2f>();
    }
    @Override
    public void shutdown()
    {
        for (TowerTexture towerTexture : mTowerTextures)
        {
            towerTexture.shutdown();
        }

        mFireTexture.shutdown();
        mWaterTexture.shutdown();
    }

    public void draw(float[] mvpMatrix)
    {
        for(Map.Entry<AbstractTower, Integer> entry : mTowers.entrySet())
        {
            Point2f position = entry.getKey().getPosition();
            Integer index = entry.getValue();

            TowerTexture texture = mTowerTextures.get(index);
            texture.setPosition(position.x, position.y);

            texture.draw(mvpMatrix.clone());
        }

        while(!mFirePositions.isEmpty())
            mFireTexture.draw(mvpMatrix.clone(), mFirePositions.remove());

        while(!mWaterPositions.isEmpty())
            mWaterTexture.draw(mvpMatrix.clone(), mWaterPositions.remove());
    }

    public void towerCreated(AbstractTower tower)
    {
        if(tower.getType() == TankTower.TYPE)
            mTowers.put(tower, 0);
        else
            mTowers.put(tower, 3);
    }
    public void towerUpgraded(AbstractTower tower)
    {
        mTowers.put(tower, mTowers.get(tower) + 1);
    }
    public void towerDestroyed(AbstractTower tower)
    {
        mTowers.remove(tower);
    }
    public void projectileUpdated(AbstractProjectile projectile, String type)
    {
        if(type == TankProjectile.TYPE)
            mFirePositions.add(projectile.getPosition());
        else
            mWaterPositions.add(projectile.getPosition());
    }
}

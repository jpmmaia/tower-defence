package Game.View.GameSessionView;

import Game.View.IView;
import Models.Textures.ButtonTexture;
import android.content.Context;
import app.TowerDefence.R;

/**
 * Created by João on 09/06/2014.
 */
public class GameStatusView implements IView
{
    ButtonTexture mGameWon;
    ButtonTexture mGameLost;
    boolean mGameEnded;

    public GameStatusView(Context context)
    {
        mGameWon = new ButtonTexture(context, R.drawable.won);
        mGameWon.setPosition(0.0f, 0.0f);
        mGameWon.setScale(0.45f, 0.2f);

        mGameLost = new ButtonTexture(context, R.drawable.lost);
        mGameLost.setPosition(0.0f, 0.0f);
        mGameLost.setScale(0.45f, 0.2f);

        mGameEnded = false;
    }
    public void initialize()
    {
        mGameWon.setVisibility(false);
        mGameLost.setVisibility(false);
        mGameEnded = false;
    }

    @Override
    public void shutdown()
    {
    }

    @Override
    public void draw(float[] mvpMatrix)
    {
        if(!mGameEnded)
            return;

        mGameWon.draw(mvpMatrix.clone());
        mGameLost.draw(mvpMatrix.clone());
    }

    public void setStatus(boolean victory)
    {
        if(victory)
            mGameWon.setVisibility(true);
        else
            mGameLost.setVisibility(true);

        mGameEnded = true;
    }
}

package Game.View.GameSessionView;

import Game.View.IView;
import Models.Textures.TextTexture;
import Terrain.Terrain;
import android.content.Context;
import app.TowerDefence.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by João on 07/06/2014.
 */
public class NumbersView implements IView
{
    List<TextTexture> mNumbers = new ArrayList<TextTexture>(10);
    List<Integer> mSequence;

    float mX;
    float mY;
    float mZ;
    float mNumberWidth = Terrain.getCellWidth() / 2.0f;
    float mNumberHeight = Terrain.getCellHeight() / 2.0f;

    public NumbersView(Context context, float x, float y, float z)
    {
        int[] resourceIds =
                {
                        R.drawable.n0, R.drawable.n1, R.drawable.n2, R.drawable.n3, R.drawable.n4,
                        R.drawable.n5, R.drawable.n6, R.drawable.n7, R.drawable.n8, R.drawable.n9
                };

        mX = x;
        mY = y;
        mZ = z;

        for (int resourceId : resourceIds)
        {
            TextTexture texture = new TextTexture(context, resourceId);
            texture.setScale(mNumberWidth, mNumberHeight);
            mNumbers.add(texture);
        }
    }
    public void initialize()
    {
        mSequence = new ArrayList<Integer>();
    }
    public void shutdown()
    {
        for (TextTexture number : mNumbers)
        {
            number.shutdown();
        }
    }

    public void draw(float[] mvpMatrix)
    {
        int size = mSequence.size();
        for (int i = 0; i < size; i++)
        {
            TextTexture texture = mNumbers.get(mSequence.get(size - i - 1));
            texture.setPosition(mX + mNumberWidth * i / 2.0f, mY, mZ);

            texture.draw(mvpMatrix.clone());
        }
    }

    public void setNumber(int number)
    {
        mSequence.removeAll(mSequence);
        if(number == 0)
        {
            mSequence.add(0);
            return;
        }

        while(number != 0)
        {
            mSequence.add(number % 10);
            number /= 10;
        }
    }
}

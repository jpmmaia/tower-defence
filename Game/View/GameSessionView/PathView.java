package Game.View.GameSessionView;

import Game.View.IView;
import Models.Textures.PathTexture;
import Terrain.Cell.Cell;
import Terrain.Cell.CellPath;
import Terrain.Cell.PathState;
import android.content.Context;
import app.TowerDefence.R;

import javax.vecmath.Point2f;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by João on 07/06/2014.
 */
public class PathView implements IView
{
    List<PathTexture> mPathTextures = new ArrayList<PathTexture>(6);
    List<Point2f> mPathPositions;
    List<Integer> mPathIndices;

    PathTexture mPathStart;
    PathTexture mPathFinish;

    public PathView(Context context)
    {
        mPathTextures.add(new PathTexture(context, R.drawable.path1_horizontal));
        mPathTextures.add(new PathTexture(context, R.drawable.path1_vertical));
        mPathTextures.add(new PathTexture(context, R.drawable.path1_bl));
        mPathTextures.add(new PathTexture(context, R.drawable.path1_br));
        mPathTextures.add(new PathTexture(context, R.drawable.path1_tl));
        mPathTextures.add(new PathTexture(context, R.drawable.path1_tr));

        mPathStart = new PathTexture(context, R.drawable.path_start);
        mPathFinish = new PathTexture(context, R.drawable.path_finish);
    }
    public void initialize(CellPath path)
    {
        int size = path.getSize();
        mPathPositions = new ArrayList<Point2f>(size);
        mPathIndices = new ArrayList<Integer>(size);

        Iterator<Cell> pathIterator = path.getIterator();
        Cell startCell = pathIterator.next();
        mPathStart.setPosition(startCell.getX(), startCell.getY(), 0.01f);

        for(int i = 1; i < size - 1; i++)
        {
            Cell cell = pathIterator.next();
            mPathPositions.add(cell.getPosition());

            switch(cell.getCorner())
            {
                case PathState.BOTTOM_LEFT:
                    mPathIndices.add(2);
                    continue;
                case PathState.BOTTOM_RIGHT:
                    mPathIndices.add(3);
                    continue;
                case PathState.TOP_LEFT:
                    mPathIndices.add(4);
                    continue;
                case PathState.TOP_RIGHT:
                    mPathIndices.add(5);
                    continue;
            }

            switch (cell.getDirection())
            {
                case PathState.EAST:
                case PathState.WEST:
                    mPathIndices.add(0);
                    continue;

                case PathState.NORTH:
                case PathState.SOUTH:
                    mPathIndices.add(1);
                    continue;
            }
        }

        Cell finishCell = pathIterator.next();
        mPathFinish.setPosition(finishCell.getX(), finishCell.getY(), 0.01f);
    }
    @Override
    public void shutdown()
    {
        mPathStart.shutdown();
        mPathFinish.shutdown();

        for (PathTexture pathTexture : mPathTextures)
        {
            pathTexture.shutdown();
        }
    }

    public void draw(float[] mvpMatrix)
    {
        mPathStart.draw(mvpMatrix.clone());
        mPathFinish.draw(mvpMatrix.clone());

        for(int i = 0; i < mPathIndices.size(); i++)
        {
            Integer index = mPathIndices.get(i);
            PathTexture pathTexture = mPathTextures.get(index);

            Point2f position = mPathPositions.get(i);
            pathTexture.setPosition(position.x, position.y, 0.01f);
            pathTexture.draw(mvpMatrix.clone());
        }
    }
}

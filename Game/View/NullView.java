package Game.View;

/**
 * Created by João on 09/06/2014.
 */
public class NullView implements IView
{
    @Override
    public void shutdown()
    {
    }

    @Override
    public void draw(float[] mvpMatrix)
    {
    }
}

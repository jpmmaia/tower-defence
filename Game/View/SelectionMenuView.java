package Game.View;

import Game.Model.Menu.IMenu;
import Game.Model.Menu.ISelectionMenuObserver;
import Models.Textures.ButtonTexture;
import Models.UserInterface.Button;
import android.content.Context;
import app.TowerDefence.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by João on 08/06/2014.
 */
public class SelectionMenuView implements IView, ISelectionMenuObserver
{
    List<ButtonTexture> mLevelsArea = new ArrayList<ButtonTexture>(3);
    int mCurrentLevel;

    ButtonTexture mLeftArrow;
    ButtonTexture mRightArrow;

    public SelectionMenuView(Context context)
    {
        mLevelsArea.add(new ButtonTexture(context, R.drawable.level_1));
        mLevelsArea.add(new ButtonTexture(context, R.drawable.level_2));
        mLevelsArea.add(new ButtonTexture(context, R.drawable.level_3));

        mLeftArrow = new ButtonTexture(context, R.drawable.arrow);
        mLeftArrow.setRotation(180.0f);

        mRightArrow = new ButtonTexture(context, R.drawable.arrow);
    }
    public void initialize(IMenu menu)
    {
        List<Button> buttons = menu.getButtons();

        Button levelArea = buttons.get(0);
        float xLevel = levelArea.getXm();
        float yLevel = levelArea.getYm();
        float widthLevel = levelArea.getWidth();
        float heightLevel = levelArea.getHeight();
        for (ButtonTexture texture : mLevelsArea)
        {
            texture.setPosition(xLevel, yLevel);
            texture.setScale(widthLevel, heightLevel);
            texture.setVisibility(true);
        }

        Button leftArrow = buttons.get(1);
        mLeftArrow.setPosition(leftArrow.getXm(), leftArrow.getYm());
        mLeftArrow.setScale(leftArrow.getWidth(), leftArrow.getHeight());
        mLeftArrow.setVisibility(true);

        Button rightArrow = buttons.get(2);
        mRightArrow.setPosition(rightArrow.getXm(), rightArrow.getYm());
        mRightArrow.setScale(rightArrow.getWidth(), rightArrow.getHeight());
        mRightArrow.setVisibility(true);
    }
    @Override
    public void shutdown()
    {
    }

    @Override
    public void draw(float[] mvpMatrix)
    {
        mLevelsArea.get(mCurrentLevel - 1).draw(mvpMatrix.clone());

        if(mCurrentLevel != 1)
            mLeftArrow.draw(mvpMatrix.clone());

        if(mCurrentLevel != 3)
            mRightArrow.draw(mvpMatrix.clone());
    }

    @Override
    public void selectionChanged(int level)
    {
        mCurrentLevel = level;
    }
}
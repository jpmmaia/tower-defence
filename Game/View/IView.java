package Game.View;

/**
 * Created by João on 07/06/2014.
 */
public interface IView
{
    void shutdown();
    void draw(float[] mvpMatrix);
}

package Game;

import Game.Model.*;
import Game.Model.GameSession.GameSession;
import Game.Model.Menu.IMenu;
import Game.View.GameSessionView.GameSessionView;
import Game.View.IView;
import Game.View.MainMenuView;
import Game.View.NullView;
import Game.View.SelectionMenuView;
import android.content.Context;
import android.media.MediaPlayer;
import app.TowerDefence.R;

/**
 * Controller (MVC Pattern)
 */
public class GameController implements IGameManagerObserver
{
    Context mContext;
    GameManager mGameManager;
    IView mCurrentView;
    MainMenuView mMainMenuView;
    SelectionMenuView mSelectionMenuView;
    GameSessionView mGameSessionView;

    MediaPlayer mMusicPlayer = null;

    public GameController(Context context)
    {
        mContext = context;
        mGameManager = GameManager.getInstance();
        mCurrentView = new NullView();

        // Adding observer:
        mGameManager.addObserver(this);
    }

    public void start()
    {
        mGameManager.startMenu();
    }
    // Update the model:
    public void update(long deltaMillis)
    {
        mGameManager.update(deltaMillis);
    }
    // Modify the model:
    public void touch(float x, float y)
    {
        mGameManager.touch(x, y);
    }
    // Draw the view:
    public void draw(float[] mvpMatrix)
    {
        mCurrentView.draw(mvpMatrix);
    }

    @Override
    public void gameSessionCreated(GameSession gameSession)
    {
        mGameSessionView.initialize(gameSession);
        mCurrentView = mGameSessionView;

        if(mMusicPlayer != null)
        {
            mMusicPlayer.stop();
            mMusicPlayer.release();
        }
        mMusicPlayer = MediaPlayer.create(mContext, R.raw.pokemon_red_blue_gym_leader_battle_music);
        mMusicPlayer.setLooping(true);
        mMusicPlayer.start();
    }
    @Override
    public void mainMenuCreated(IMenu menu)
    {
        mMainMenuView.initialize(menu);
        mCurrentView = mMainMenuView;

        if(mMusicPlayer != null)
        {
            mMusicPlayer.setLooping(false);
            mMusicPlayer.stop();
            mMusicPlayer.release();
        }
        mMusicPlayer = MediaPlayer.create(mContext, R.raw.pokemon_red_blue_opening);
        mMusicPlayer.setLooping(true);
        mMusicPlayer.start();
    }
    @Override
    public void selectionMenuCreated(IMenu menu)
    {
        mSelectionMenuView.initialize(menu);
        mCurrentView = mSelectionMenuView;
    }

    public void setMainMenuView(MainMenuView view)
    {
        mMainMenuView = view;
    }
    public void setSelectionMenuView(SelectionMenuView view)
    {
        mSelectionMenuView = view;
        mGameManager.addSelectionMenuObserver(mSelectionMenuView);
    }
    public void setGameSessionView(GameSessionView view)
    {
        mGameSessionView = view;
        mGameManager.addGameSessionObserver(mGameSessionView);
    }

    public void shutdown()
    {
        mMainMenuView.shutdown();
        mSelectionMenuView.shutdown();
        mGameSessionView.shutdown();

        if(mMusicPlayer != null)
        {
            mMusicPlayer.setLooping(false);
            mMusicPlayer.stop();
            mMusicPlayer.release();
            mMusicPlayer = null;
        }
    }
}

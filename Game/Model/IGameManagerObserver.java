package Game.Model;

import Game.Model.GameSession.GameSession;
import Game.Model.Menu.IMenu;

/**
 * Informs controller and view for GameManager significant changes
 * Observer pattern
 */
public interface IGameManagerObserver
{
    /**
     * Informs the observers that a new level started
     * @param gameSession
     */
    void gameSessionCreated(GameSession gameSession);

    /**
     * Informs the observers that the MainMenu was created
     * @param menu
     */
    void mainMenuCreated(IMenu menu);

    /**
     * Informs the observers the SelectionMenu was created
     * @param menu
     */
    void selectionMenuCreated(IMenu menu);
}

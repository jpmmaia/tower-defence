package Game.Model.Menu;

/**
 * Interface used by the view to observe events on the selection menu
 * Observer pattern
 */
public interface ISelectionMenuObserver
{
    void selectionChanged(int level);
}

package Game.Model.Menu;

import Models.UserInterface.Button;

import java.util.List;

/**
 * Interface which every menu implements
 */
public interface IMenu
{
    /**
     * Called when the user touches the menu
     * @param x
     * @param y
     */
    void touch(float x, float y);

    /**
     * To inform the view of the position and dimensions of the menu buttons
     * @return
     */
    List<Button> getButtons();

    /**
     * To inform the type of the menu
     * @return
     */
    String getType();
}

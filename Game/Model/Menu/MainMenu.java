package Game.Model.Menu;

import Game.Model.MenuState;
import Models.UserInterface.Button;
import Models.UserInterface.IButtonObserver;

import java.util.ArrayList;
import java.util.List;

/**
 * Main Menu class
 */
public class MainMenu implements IMenu, IButtonObserver
{
    public static final String TYPE = "Main Menu";

    MenuState mMenuState;
    Button mStartButton;

    /**
     * Main Menu controller
     * @param menuState
     */
    public MainMenu(MenuState menuState)
    {
        mMenuState = menuState;

        float width = 0.675f;
        float height = 0.3f;
        float xi = -width / 2.0f;
        float yi = -1.0f + height / 2.0f;

        mStartButton = new Button(xi, -0.85f, width, height);
        mStartButton.addObserver(this);
    }

    /**
     * Called when the user touches this menu
     * @param x
     * @param y
     */
    public void touch(float x, float y)
    {
        mStartButton.touch(x, y);
    }

    @Override
    public void onTouchEvent(Button button)
    {
        if(button == mStartButton)
            mMenuState.startSelection();
    }

    @Override
    public List<Button> getButtons()
    {
        List<Button> buttons = new ArrayList<Button>(1);
        buttons.add(mStartButton);

        return buttons;
    }
    @Override
    public String getType()
    {
        return TYPE;
    }
}

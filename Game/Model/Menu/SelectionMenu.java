package Game.Model.Menu;

import Game.Model.MenuState;
import Models.UserInterface.Button;
import Models.UserInterface.IButtonObserver;

import java.util.ArrayList;
import java.util.List;

/**
 * Selection Menu class
 * Responsible for controlling the selection menu
 */
public class SelectionMenu implements IMenu, IButtonObserver
{
    public static final String TYPE = "Selection Menu";

    MenuState mMenuState;
    Button mLevelArea;
    Button mLeftArrow;
    Button mRightArrow;
    int mCurrentLevel;
    int mMaxLevel = 3;

    /**
     * Selection Menu Constructor
     * @param menuState
     */
    public SelectionMenu(MenuState menuState)
    {
        mMenuState = menuState;

        float width = 0.1125f;
        float height = 0.2f;

        mLeftArrow = new Button(-0.9f, -height / 2.0f, width, height);
        mLeftArrow.addObserver(this);

        mRightArrow = new Button(0.9f - width, -height / 2.0f, width, height);
        mRightArrow.addObserver(this);

        mLevelArea = new Button(-0.8f, -0.8f, 1.6f, 1.6f);
        mLevelArea.addObserver(this);

        setCurrentLevel(1);
    }

    @Override
    public void touch(float x, float y)
    {
        mLevelArea.touch(x, y);
        mLeftArrow.touch(x, y);
        mRightArrow.touch(x, y);
    }

    @Override
    public void onTouchEvent(Button button)
    {
        if(button == mLevelArea)
        {
            mLeftArrow.enable(false);
            mRightArrow.enable(false);
            mMenuState.startLevel(mCurrentLevel);
        }
        else if(button == mLeftArrow)
            setCurrentLevel(mCurrentLevel - 1);
        else if(button == mRightArrow)
            setCurrentLevel(mCurrentLevel + 1);
    }

    @Override
    public List<Button> getButtons()
    {
        List<Button> buttons = new ArrayList<Button>(3);
        buttons.add(mLevelArea);
        buttons.add(mLeftArrow);
        buttons.add(mRightArrow);

        return buttons;
    }

    @Override
    public String getType()
    {
        return TYPE;
    }

    /**
     * Sets the current level selected and informs observers (view)
     * @param currentLevel
     */
    private void setCurrentLevel(int currentLevel)
    {
        mCurrentLevel = currentLevel;

        mMenuState.notifySelectionChanged(mCurrentLevel);

        if(mCurrentLevel == 1)
            mLeftArrow.enable(false);
        else
            mLeftArrow.enable(true);

        if(mCurrentLevel == mMaxLevel)
            mRightArrow.enable(false);
        else
            mRightArrow.enable(true);
    }
}

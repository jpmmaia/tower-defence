package Game.Model;

/**
 * To be implemented...
 */
public class PauseState implements IGameState
{
    public static final String TYPE = "Pause State";

    @Override
    public void update(long deltaMillis)
    {
    }
    @Override
    public void touch(float x, float y)
    {
        // TODO
    }
    @Override
    public String getType()
    {
        return TYPE;
    }
}
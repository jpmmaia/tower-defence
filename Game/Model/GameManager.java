package Game.Model;

import Game.Model.GameSession.GameSession;
import Game.Model.GameSession.IGameSessionObserver;
import Game.Model.Menu.IMenu;
import Game.Model.Menu.ISelectionMenuObserver;
import Game.Model.Menu.MainMenu;
import Terrain.Cell.Cell;
import Tower.AbstractTowerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Game Manager
 * Facade & Singleton & State patterns
 * Simplifies the whole program.
 * The view and controller only need to communicate with this class
 */
public class GameManager
{
    static GameManager mInstance = new GameManager();
    List<IGameManagerObserver> mObservers = new ArrayList<IGameManagerObserver>();
    List<ISelectionMenuObserver> mSelectionMenuObservers = new ArrayList<ISelectionMenuObserver>();
    List<IGameSessionObserver> mGameSessionObservers = new ArrayList<IGameSessionObserver>();
    IGameState mGameState = new MenuState(this);
    GameSession mGameSession = null;

    /**
     * Private GameManager Constructor (Singleton)
     */
    private GameManager(){}

    /**
     * Changes to the MenuState
     */
    public void startMenu()
    {
        mGameState = new MenuState(this);
    }

    /**
     * Changes to the RunningLevelState
     * @param level
     */
    public void startLevel(int level)
    {
        mGameState = new RunningLevelState(this, level);
    }

    /**
     * Updates the game state
     * @param deltaMillis
     */
    public void update(long deltaMillis)
    {
        mGameState.update(deltaMillis);
    }

    /**
     * Changes the model with user touch coordinates
     * @param x
     * @param y
     */
    public void touch(float x, float y)
    {
        mGameState.touch(x, y);
    }

    /**
     * A new tower is created in the specified cell
     * @param cell
     * @param factory
     */
    public void createTower(Cell cell, AbstractTowerFactory factory)
    {
        mGameSession.createTower(cell, factory);
    }

    /**
     * Upgrades the tower
     * @param cell
     */
    public void upgradeTower(Cell cell)
    {
        mGameSession.upgradeTower(cell);
    }

    /**
     * Sells the tower
     * @param cell
     */
    public void sellTower(Cell cell)
    {
        mGameSession.sellTower(cell);
    }

    /**
     * Checks if the cell has a tower which can be upgraded
     * @param cell
     * @return
     */
    public boolean isUpgradeableTower(Cell cell)
    {
        return cell.getTower().isUpgradeable();
    }

    /**
     * Get the current selected cell
     * @return
     */
    public Cell getSelectedCell()
    {
        return mGameSession.getSelectedCell();
    }

    /**
     * Get the cell at the specified location
     * @param x
     * @param y
     * @return
     */
    public Cell getCell(float x, float y)
    {
        return mGameSession.getCell(x, y);
    }

    /**
     * Add a GameManager observer
     * @param observer
     */
    public void addObserver(IGameManagerObserver observer)
    {
        mObservers.add(observer);
    }

    /**
     * Add a GameSession observer
     * @param gameSessionObserver
     */
    public void addGameSessionObserver(IGameSessionObserver gameSessionObserver)
    {
        mGameSessionObservers.add(gameSessionObserver);
    }

    /**
     * Add a SelectionMenu observer
     * @param selectionMenuObserver
     */
    public void addSelectionMenuObserver(ISelectionMenuObserver selectionMenuObserver)
    {
        mSelectionMenuObservers.add(selectionMenuObserver);
    }

    /**
     * Notifies GameManager observers that a new GameSession was created
     * @param gameSession
     */
    public void notifyGameSessionCreated(GameSession gameSession)
    {
        mGameSession = gameSession;

        for (IGameSessionObserver observer : mGameSessionObservers)
        {
            gameSession.addObserver(observer);
        }
        for (IGameManagerObserver observer : mObservers)
        {
            observer.gameSessionCreated(gameSession);
        }
    }

    /**
     * Notifies GameManager observers that the MainMenu was created
     * @param menu
     */
    public void notifyMainMenuCreated(MainMenu menu)
    {
        for (IGameManagerObserver observer : mObservers)
        {
            observer.mainMenuCreated(menu);
        }
    }

    /**
     * Notifies the GameManager observers that the SelectionMenu was created
     * @param menu
     */
    public void notifySelectionMenuCreated(IMenu menu)
    {
        for (IGameManagerObserver observer : mObservers)
        {
            observer.selectionMenuCreated(menu);
        }
    }

    /**
     * Notifies the SelectionMenu observers that the SelectionMenu changed
     * @param level
     */
    public void notifySelectionChanged(int level)
    {
        for (ISelectionMenuObserver observer : mSelectionMenuObservers)
        {
            observer.selectionChanged(level);
        }
    }

    /**
     * To be used by testing classes to specify the required available money
     * @param money
     */
    public void notifyMoneyChanged(int money)
    {
        mGameSession.notifyMoneyChanged(money);
    }

    /**
     * Return the single instance of the class
     * @return
     */
    public static GameManager getInstance()
    {
        return mInstance;
    }
}
package Game.Model.GameSession;

import Game.Model.RunningLevelState;
import Models.UserInterface.CellContext;
import Models.UserInterface.NullCellContext;
import Target.IMonsterObserver;
import Target.Monster;
import Terrain.Cell.Cell;
import Terrain.Cell.CellPath;
import Terrain.Terrain;
import Terrain.TerrainBuilder.TerrainBuilder;
import Terrain.TerrainBuilder.TerrainBuilder1;
import Terrain.TerrainBuilder.TerrainBuilder2;
import Terrain.TerrainBuilder.TerrainBuilder3;
import Tower.*;

import javax.vecmath.Point2f;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * A game session is created for each level.
 * Observer pattern
 *
 * It is responsible for updating all the objects in the game and
 *spawning monsters.
 */
public class GameSession implements IMonsterObserver, IProjectileObserver
{
    List<IGameSessionObserver> mObservers = new ArrayList<IGameSessionObserver>(1);
    RunningLevelState mRunningLevelState;
    Terrain mTerrain;
    CellPath mPath;
    CellContext mCellContext = new NullCellContext();
    List<AbstractTower> mTowers = new ArrayList<AbstractTower>();
    List<Monster> mMonsters = new ArrayList<Monster>(10);
    int mMoney;
    int mLives;
    int mTotalWaves;
    int mMonstersPerWave;
    int mCurrentWave;
    int mCurrentMonster;

    /**
     * Game Session constructor
     * @param runningLevelState
     * @param level
     */
    public GameSession(RunningLevelState runningLevelState, int level)
    {
        mRunningLevelState = runningLevelState;

        createTerrain(level);
        mPath = mTerrain.getPath();
        Monster.setPath(mPath);

        mMonstersPerWave = 10;
        mTotalWaves = 5;
    }

    /**
     * Receives user input coordinates
     * @param x
     * @param y
     */
    public void touch(float x, float y)
    {
        if(mCellContext.isEnabled())
        {
            mCellContext.touch(x, y);
            notifyCellUnselected();
        }
        else
        {
            Cell cell = mTerrain.select(x, y);
            mCellContext = new CellContext(cell);
            notifyCellSelected(mCellContext, cell);
        }
    }
    public void update(long deltaMillis)
    {
        if(mMonsters.isEmpty())
        {
            if(mTotalWaves == mCurrentWave)
            {
                notifyGameWon();
                return;
            }

            spawnMonsterWave(mCurrentWave);
            notifyNewSpawnWave(mCurrentWave + 1);
        }

        Iterator<Monster> m = mMonsters.iterator();
        while(m.hasNext())
        {
            Monster monster = m.next();
            monster.update(deltaMillis);

            if(monster.isDestroyed())
                m.remove();
        }

        Monster target = null;
        if(!mMonsters.isEmpty())
            target = mMonsters.get(0);
        for(AbstractTower tower : mTowers)
        {
            tower.update(deltaMillis, target);
        }
    }
    private void spawnMonsterWave(int waveNumber)
    {
        for(int i = 0; i < mMonstersPerWave; i++)
        {
            Monster monster = new Monster(20 * (waveNumber + 1));
            monster.setSpawnTime(2000.0f * i);
            monster.addObserver(this);
            mMonsters.add(i, monster);
        }

        mCurrentMonster = 0;
    }

    public void createTower(Cell cell, AbstractTowerFactory factory)
    {
        AbstractTower tower = factory.createTower(new Level1(), cell.getPosition());
        if(mMoney < tower.getBaseCost())
            return;

        cell.addTower(tower);
        tower.addProjectileObserver(this);

        mTowers.add(tower);
        notifyMoneyChanged(mMoney - tower.getBaseCost());

        for (IGameSessionObserver observer : mObservers)
        {
            observer.towerCreated(tower);
        }
    }
    public void upgradeTower(Cell cell)
    {
        AbstractTower tower = cell.getTower();
        int upgradeMoney = tower.getUpgradeCost();
        if(mMoney < upgradeMoney)
            return;

        tower.upgrade();
        notifyMoneyChanged(mMoney - upgradeMoney);

        for (IGameSessionObserver observer : mObservers)
        {
            observer.towerUpgraded(tower);
        }
    }
    public void sellTower(Cell cell)
    {
        AbstractTower tower = cell.getTower();
        cell.destroyTower();

        mTowers.remove(tower);
        notifyMoneyChanged(mMoney + tower.getSellCost());

        for (IGameSessionObserver observer : mObservers)
        {
            observer.towerDestroyed(tower);
        }
    }

    public void notifyGameWon()
    {
        for (IGameSessionObserver observer : mObservers)
        {
            observer.gameEnded(true);
        }

        mRunningLevelState.stop();
    }
    public void notifyGameLost()
    {
        for (IGameSessionObserver observer : mObservers)
        {
            observer.gameEnded(false);
        }

        mRunningLevelState.stop();
    }
    public void notifyCellSelected(CellContext cellContext, Cell cell)
    {
        for (IGameSessionObserver observer : mObservers)
        {
            observer.cellSelected(cellContext, cell);
        }
    }
    public void notifyCellUnselected()
    {
        for(IGameSessionObserver observer : mObservers)
        {
            observer.cellUnselected();
        }
    }
    public void notifyMoneyChanged(int money)
    {
        mMoney = money;

        for (IGameSessionObserver observer : mObservers)
        {
            observer.moneyChanged(money);
        }
    }
    public void notifyNewSpawnWave(int currentWave)
    {
        mCurrentWave = currentWave;

        for (IGameSessionObserver observer : mObservers)
        {
            observer.newSpawnWave(currentWave);
        }
    }
    public void notifyLivesChanged(int lives)
    {
        mLives = lives;

        for (IGameSessionObserver observer : mObservers)
        {
            observer.livesChanged(lives);
        }

        if(mLives == 0)
            notifyGameLost();
    }

    @Override
    public void monsterUpdated(Point2f position)
    {
        for (IGameSessionObserver observer : mObservers)
        {
            observer.monsterUpdated(position);
        }
    }
    @Override
    public void monsterDied(Monster monster)
    {
        notifyMoneyChanged(mMoney + 1);
    }
    @Override
    public void monsterEscaped(Monster monster)
    {
        notifyLivesChanged(mLives - 1);
    }
    @Override
    public void projectileUpdated(AbstractProjectile projectile, String type)
    {
        for (IGameSessionObserver observer : mObservers)
        {
            observer.projectileUpdated(projectile, type);
        }
    }

    public void addObserver(IGameSessionObserver observer)
    {
        mObservers.add(observer);
    }
    public Cell getCell(float x, float y)
    {
        return mTerrain.getCell(x, y);
    }
    public Cell getSelectedCell()
    {
        return mTerrain.getSelected();
    }
    public CellPath getPath()
    {
        return mPath;
    }
    public int getTotalWaves()
    {
        return mTotalWaves;
    }

    private void createTerrain(int level)
    {
        if(level < 1 || level > 3)
            return;

        TerrainBuilder builder = null;
        switch (level)
        {
            case 1:
                builder = new TerrainBuilder1();
                break;
            case 2:
                builder = new TerrainBuilder2();
                break;
            case 3:
                builder = new TerrainBuilder3();
                break;
        }

        builder.create();
        mTerrain = builder.getTerrain();
    }
}

package Game.Model.GameSession;

import Models.UserInterface.CellContext;
import Terrain.Cell.Cell;
import Tower.AbstractProjectile;
import Tower.AbstractTower;

import javax.vecmath.Point2f;

/**
 * Interface for GameSession observers (Observer pattern)
 * Observer pattern
 *
 * Mostly used to inform the view of the events that occured
 */
public interface IGameSessionObserver
{
    /**
     * Informating observers that the game ended
     * @param victory
     */
    void gameEnded(boolean victory);

    /**
     * Informing observers that a moster has moved
     * @param position
     */
    void monsterUpdated(Point2f position);

    /**
     * Informing observers that a cell was selected
     * @param cellContext
     * @param cell
     */
    void cellSelected(CellContext cellContext, Cell cell);

    /**
     * Informing observers that the cell was unselected
     */
    void cellUnselected();

    /**
     * Informing that a tower was created
     * @param tower
     */
    void towerCreated(AbstractTower tower);

    /**
     * Informing that a tower was upgraded
     * @param tower
     */
    void towerUpgraded(AbstractTower tower);

    /**
     * Informing that a tower was destroyed
     * @param tower
     */
    void towerDestroyed(AbstractTower tower);

    /**
     * Informing that the money changed
     * @param money
     */
    void moneyChanged(int money);

    /**
     * Informing that a projectile has moved
     * @param projectile
     * @param type
     */
    void projectileUpdated(AbstractProjectile projectile, String type);

    /**
     * Informing that a new new monsters wave has been created
     * @param currentWave
     */
    void newSpawnWave(int currentWave);

    /**
     * Informing that the player remaining lives changed
     * @param lives
     */
    void livesChanged(int lives);
}

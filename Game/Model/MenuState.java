package Game.Model;

import Game.Model.Menu.IMenu;
import Game.Model.Menu.MainMenu;
import Game.Model.Menu.SelectionMenu;

/**
 * State responsible for the menu management
 * State Pattern
 */
public class MenuState implements IGameState
{
    public static final String TYPE = "Menu State";

    GameManager mGameManager;

    IMenu mCurrentMenu;
    MainMenu mMainMenu;
    SelectionMenu mSelectionMenu;

    /**
     * Menu State constructor
     * @param gameManager
     */
    public MenuState(GameManager gameManager)
    {
        mGameManager = gameManager;

        mMainMenu = new MainMenu(this);
        mCurrentMenu = mMainMenu;
        mGameManager.notifyMainMenuCreated(mMainMenu);

        mSelectionMenu = new SelectionMenu(this);
    }

    /**
     * Notifies that the selection menu is to be displayed
     */
    public void startSelection()
    {
        mCurrentMenu = mSelectionMenu;
        mGameManager.notifySelectionMenuCreated(mSelectionMenu);
    }

    /**
     * Notifies that the a new level is to be played
     * @param level
     */
    public void startLevel(int level)
    {
        mGameManager.startLevel(level);
    }

    /**
     * Notifies that the selection menu was updated
     * @param level
     */
    public void notifySelectionChanged(int level)
    {
        mGameManager.notifySelectionChanged(level);
    }

    @Override
    public void update(long deltaMillis)
    {
    }
    @Override
    public void touch(float x, float y)
    {
        mCurrentMenu.touch(x, y);
    }
    @Override
    public String getType()
    {
        return TYPE;
    }
}
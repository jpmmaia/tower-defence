package Game.Model;

/**
 * Interface for all the GameManager possible states
 * State Pattern
 */
public interface IGameState
{
    /**
     * Updates the state
     * @param deltaMillis
     */
    public void update(long deltaMillis);

    /**
     * User input
     * @param x
     * @param y
     */
    public void touch(float x, float y);

    /**
     * Returns the type of state (Menu, Pause or RunningLevel)
     * @return
     */
    public String getType();
}
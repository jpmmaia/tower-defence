package Game.Model;

import Game.Model.GameSession.GameSession;

/**
 * Responsible for creating the GameSession
 */
public class RunningLevelState implements IGameState
{
    public static final String TYPE = "Running Level State";

    GameManager mGameManager;
    GameSession mGameSession;
    boolean mRunning;
    long mCurrentTick;

    /**
     * Creates GameSession and notifies observers of the initial variable values
     * @param gameManager
     * @param level
     */
    public RunningLevelState(GameManager gameManager, int level)
    {
        mGameManager = gameManager;
        mGameSession = new GameSession(this, level);

        gameManager.notifyGameSessionCreated(mGameSession);
        mGameSession.notifyMoneyChanged(10);
        mGameSession.notifyLivesChanged(5);
        mGameSession.notifyNewSpawnWave(0);

        mRunning = true;
    }

    /**
     * Stop updating
     */
    public void stop()
    {
        mRunning = false;
    }

    @Override
    public void update(long deltaMillis)
    {
        if(!mRunning)
            return;

        mCurrentTick += deltaMillis;

        if(mCurrentTick > 3000)
            mGameSession.update(deltaMillis);
    }
    @Override
    public void touch(float x, float y)
    {
        // If the game ended, then it waits for the next touch to go to the menu:
        if(!mRunning)
            mGameManager.startMenu();

        mGameSession.touch(x, y);
    }
    @Override
    public String getType()
    {
        return TYPE;
    }
}
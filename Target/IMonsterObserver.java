package Target;

import javax.vecmath.Point2f;

/**
 * Observer pattern
 */
public interface IMonsterObserver
{
    /**
     * Called when a monster position is updated
     * @param position
     */
    void monsterUpdated(Point2f position);

    /**
     * Called when a monster dies when hit by a projectile
     * @param monster
     */
    void monsterDied(Monster monster);

    /**
     * Called when a monster reaches the end of the path
     * @param monster
     */
    void monsterEscaped(Monster monster);
}

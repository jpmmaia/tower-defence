package Target;

import Tower.AbstractProjectile;

import javax.vecmath.Point2f;

/**
 * Interface to be implemented by the monster and obstacle.
 * Visitor pattern -> Abstract projectile
 * The classes that implement this interface can be damaged
 *by the projectiles.
 */
public interface IDamageable
{
    /**
     * Used for collision (Visitor pattern)
     * @param projectile
     */
    public void accept(AbstractProjectile projectile);

    /**
     * Get target current heath
     * @return
     */
    public int getHealth();
    public Point2f getPosition();
    public void damage(int value);
    public void update(long deltaMillis);
    public boolean isDestroyed();
}
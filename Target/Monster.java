package Target;

import Terrain.Cell.CellPath;
import Terrain.Cell.NullCellPath;
import Tower.AbstractProjectile;

import javax.vecmath.Point2f;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Jo�o
 * @version 1.0
 * @created 21-mai-2014 11:52:00
 */
public class Monster implements IDamageable
{
    static CellPath mPath = new NullCellPath();
    List<IMonsterObserver> mObservers = new ArrayList<IMonsterObserver>(1);

    Point2f mPosition = new Point2f(0.0f, 0.0f);
    int mHealth = 100;
    float mVelocity = 0.5f;
    float mDistance = 0.0f;

    boolean mDestroyed = false;
    boolean mVisible = false;
    float mSpawnTime = 0.0f;

    public Monster()
    {
        mPosition = mPath.getStartPoint();
    }
    public Monster(int health)
    {
        mHealth = health;
        mPosition = mPath.getStartPoint();
    }
    public Monster(int health, Point2f position)
    {
        mHealth = health;
        mPosition = position;
    }

    @Override
    public void update(long deltaMillis)
    {
        if(mDestroyed)
            return;

        if(!mVisible)
        {
            mSpawnTime -= deltaMillis;
            if(mSpawnTime <= 0.0f)
                mVisible = true;
            return;
        }

        mDistance += (float)deltaMillis * mVelocity / 1000.0f;
        if(mPath.isAtEnd(mDistance))
        {
            mDestroyed = true;
            mVisible = false;
            notifyMonsterEscaped();
            return;
        }

        mPosition = mPath.getPosition(mDistance);

        notifyMonsterUpdated();
    }
    @Override
    public void accept(AbstractProjectile projectile)
    {
        projectile.visit(this);
    }
    @Override
    public void damage(int value)
    {
        if(isDestroyed())
            return;

        mHealth -= value;

        if(mHealth < 0)
        {
            mHealth = 0;
            mDestroyed = true;
            mVisible = false;

            notifyMonsterDied();
        }
    }
    @Override
    public int getHealth()
    {
        return mHealth;
    }
    @Override
    public Point2f getPosition()
    {
        return mPosition;
    }
    @Override
    public boolean isDestroyed()
    {
        return mDestroyed;
    }

    public void notifyMonsterUpdated()
    {
        for (IMonsterObserver observer : mObservers)
        {
            observer.monsterUpdated(mPosition);
        }
    }
    public void notifyMonsterDied()
    {
        for (IMonsterObserver observer : mObservers)
        {
            observer.monsterDied(this);
        }
    }
    public void notifyMonsterEscaped()
    {
        for (IMonsterObserver observer : mObservers)
        {
            observer.monsterEscaped(this);
        }
    }

    public void setVisibility(boolean visible)
    {
        mVisible = visible;
    }
    public void setSpawnTime(float spawnTime)
    {
        mSpawnTime = spawnTime;
    }

    public void addObserver(IMonsterObserver observer)
    {
        mObservers.add(observer);
    }
    public static void setPath(CellPath path)
    {
        mPath = path;
    }
}
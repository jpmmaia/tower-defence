package Target;

import Tower.AbstractProjectile;

import javax.vecmath.Point2f;

/**
 * @author Jo�o
 * @version 1.0
 * @created 21-mai-2014 11:52:00
 */
public class Obstacle implements IDamageable
{
    Point2f mPosition = new Point2f(0.0f, 0.0f);
    int mHealth = 100;
    boolean mDestroyed = false;

    public Obstacle()
    {
    }

    public Obstacle(int health)
    {
        mHealth = health;
    }

    public Obstacle(int health, Point2f position)
    {
        mHealth = health;
        mPosition = position;
    }

    /**
     * @param projectile
     */
    @Override
    public void accept(AbstractProjectile projectile)
    {
        projectile.visit(this);
    }

    @Override
    public int getHealth()
    {
        return mHealth;
    }

    @Override
    public Point2f getPosition()
    {
        return mPosition;
    }

    @Override
    public void damage(int value)
    {
        mHealth -= value;

        if(mHealth < 0)
            mHealth = 0;
    }

    @Override
    public void update(long deltaMillis)
    {
    }

    @Override
    public boolean isDestroyed()
    {
        return mDestroyed;
    }
}